"""
This script will generate BBCODE for statistics from a specified week in a tournament.
It can be run either on its own - in which case it will simply print the code for all
available stats on the screen - or with command line arguments, if only a specific
subset of the stats is desired.

Please edit the following variables in __init.__py accordingly:

TOURNAMENT_NAME = name of the current tournament
WEEK_NUMBER = current week number

Available command line arguments:

-m: generates only match stats
-c: generates only congratulations
-e: generates only elite stats
"""

from __init__ import TOURNAMENT_NAME, WEEK_NUMBER, DIV_A, DIV_B, DIV_C
from BBCodeGen import TableWithPlayersGenerator
from BBCodeGen import StatisticTableGenerator
from BBCodeGen import ThemeTableGenerator
from BBCodeGen import CongratsSectionGenerator
from BBCodeGen import MatchStatsComposer
from PROVIDERS.MatchStatsProvider import MatchStatsProvider
from PROVIDERS.TournamentProvider import TournamentProvider
import sys


class WeeklyStatsGenerator():
    def __init__(self, tournament_name, week_number):
        tournament_provider = TournamentProvider()
        self.tournament = tournament_provider.provide_tournament(tournament_name)
        self.week_number = week_number
        self.stats_provider = MatchStatsProvider()

    def generate_match_stats(self):
        match_stats = self.stats_provider.provide_match_stats_from_round(self.week_number, self.tournament.tournament_id)

        return MatchStatsComposer.generate_whole_week(match_stats, self.week_number)

    def generate_elite_stats(self):
        stats = self.stats_provider.provide_elite_stats(self.tournament.tournament_id, self.week_number)

        forum_tables = list()
        for stat in stats:
            content = TableWithPlayersGenerator.generate(stat.statistic_type, stat.stats)
            forum_tables.append(StatisticTableGenerator.generate(stat.title, stat.subtitle, content))

        return ThemeTableGenerator.generate(number_of_columns=3, cells=forum_tables)

    def generate_congrats_section(self):
        star_of_the_week, congratulations = self.stats_provider.provide_congrats_section_data(
            self.tournament.tournament_id,
            self.week_number,
            DIV_A)

        congrats_div_b = self.stats_provider.provide_congrats_section_data(self.tournament.tournament_id, self.week_number, DIV_B)[1]
        congrats_div_c = self.stats_provider.provide_congrats_section_data(self.tournament.tournament_id, self.week_number, DIV_C)[1]

        congratulations[0].names.extend(congrats_div_b[0].names)
        congratulations[0].names.extend(congrats_div_c[0].names)
        congratulations[1].names.extend(congrats_div_b[1].names)
        congratulations[1].names.extend(congrats_div_c[1].names)
        congratulations[2].names.extend(congrats_div_b[2].names)
        congratulations[2].names.extend(congrats_div_c[2].names)

        return CongratsSectionGenerator.generate(star_of_the_week, congratulations)


if __name__ == "__main__":
    generator = WeeklyStatsGenerator(TOURNAMENT_NAME, WEEK_NUMBER)

    if len(sys.argv) > 1:
        if sys.argv[1] == "-m":
            print(generator.generate_match_stats())
            sys.exit(0)

        elif sys.argv[1] == "-c":
            print(generator.generate_congrats_section())
            sys.exit(0)

        elif sys.argv[1] == "-e":
            print(generator.generate_elite_stats())
            sys.exit(0)

        else:
            print("Invalid option. Use:")
            print("-m for match stats")
            print("-c for congratulations")
            print("-e for elite stats")
            sys.exit(0)

    print(generator.generate_match_stats())
    print("\n\n")
    print(generator.generate_congrats_section())
    print("\n\n")
    print(generator.generate_elite_stats())
