from sqlalchemy.sql import text
from mnbmodel import db as DB
from .DbUtils import DbUtils

database = DB.Database('mysql+mysqlconnector://root:Buju747!@localhost/mnbstats')


class MapAndFactionsStatsRetriever():
    def __init__(self):
        pass

    def find_fixtures(self, tournament_id):
        with database.engine.connect() as connection:
            query = text("SELECT DISTINCT map, attacking_faction, defending_faction "
                         "FROM Spawns S "
                             "JOIN Matches M ON S.match_id=M.match_id "
                         "WHERE tournament_id = :tour_id ")

            result = connection.execute(query, tour_id=tournament_id)

            return result.fetchall()

    def find_faction_list(self, tournament_id):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(attacking_faction) "
                         "FROM Spawns S "
                             "JOIN Matches M ON S.match_id=M.match_id "
                         "WHERE tournament_id = :tour_id "
                         "UNION "
                         "SELECT distinct(defending_faction) "
                         "FROM Spawns S "
                             "JOIN Matches M ON S.match_id=M.match_id "
                         "WHERE tournament_id = :tour_id")

            result = connection.execute(query, tour_id=tournament_id)

            return DbUtils.fetch_list(result)

    def find_attacking_faction_kills(self, tournament_id, map, a_fac, d_fac):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) AS kills "
                         "FROM Kills K "
                             "JOIN Spawns S ON K.match_id=S.match_id "
                                 "AND K.spawn=S.spawn "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                         "WHERE tournament_id = :tour_id "
                             "AND map = :map "
                             "AND attacking_faction = :a_fac "
                             "AND defending_faction = :d_fac "
                             "AND team_id = attacking_team "
                             "AND teamkill = 0 ")

            result = connection.execute(query, tour_id=tournament_id, map=map, a_fac=a_fac, d_fac=d_fac)

            return DbUtils.fetch_one_value(result)

    def find_attacking_faction_deaths(self, tournament_id, map, a_fac, d_fac):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) AS kills "
                         "FROM Kills K "
                             "JOIN Spawns S ON K.match_id=S.match_id "
                                 "AND K.spawn=S.spawn "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON K.victim_id=TTP.player_id "
                         "WHERE tournament_id = :tour_id "
                             "AND map = :map "
                             "AND attacking_faction = :a_fac "
                             "AND defending_faction = :d_fac "
                             "AND team_id = attacking_team ")

            result = connection.execute(query, tour_id=tournament_id, map=map, a_fac=a_fac, d_fac=d_fac)

            return DbUtils.fetch_one_value(result)

    def find_attacking_faction_melee_kills(self, tournament_id, map, a_fac, d_fac):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) AS melee_kills "
                         "FROM Kills K "
                             "JOIN Spawns S ON K.match_id=S.match_id "
                                 "AND K.spawn=S.spawn "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                             "JOIN MatchPlayers MP ON K.killer_id=MP.player_id "
                                 "AND K.match_id=MP.match_id "
                                 "AND K.spawn=MP.spawn "
                                 "AND K.round=MP.round "
                         "WHERE (wb_class = 'Sergeant' OR wb_class = 'Infantry' OR wb_class = 'Footman' OR wb_class = 'Huscarl' OR wb_class = 'Spearman') "
                         "AND tournament_id = :tour_id "
                             "AND map = :map "
                             "AND attacking_faction = :a_fac "
                             "AND defending_faction = :d_fac "
                             "AND team_id = attacking_team "
                             "AND teamkill = '0' ")

            result = connection.execute(query, tour_id=tournament_id, map=map, a_fac=a_fac, d_fac=d_fac)

            return DbUtils.fetch_one_value(result)

    def find_attacking_faction_ranged_kills(self, tournament_id, map, a_fac, d_fac):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) AS melee_kills "
                         "FROM Kills K "
                             "JOIN Spawns S ON K.match_id=S.match_id "
                                 "AND K.spawn=S.spawn "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                             "JOIN MatchPlayers MP ON K.killer_id=MP.player_id "
                                 "AND K.match_id=MP.match_id "
                                 "AND K.spawn=MP.spawn "
                                 "AND K.round=MP.round "
                         "WHERE (wb_class = 'Archer' OR wb_class = 'Crossbowman') "
                             "AND tournament_id = :tour_id "
                             "AND map = :map "
                             "AND attacking_faction = :a_fac "
                             "AND defending_faction = :d_fac "
                             "AND team_id = attacking_team "
                             "AND teamkill = '0' ")

            result = connection.execute(query, tour_id=tournament_id, map=map, a_fac=a_fac, d_fac=d_fac)

            return DbUtils.fetch_one_value(result)

    def find_attacking_faction_cav_kills(self, tournament_id, map, a_fac, d_fac):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) AS melee_kills "
                         "FROM Kills K "
                             "JOIN Spawns S ON K.match_id=S.match_id "
                                 "AND K.spawn=S.spawn "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                             "JOIN MatchPlayers MP ON K.killer_id=MP.player_id "
                                 "AND K.match_id=MP.match_id "
                                 "AND K.spawn=MP.spawn "
                                 "AND K.round=MP.round "
                         "WHERE (wb_class = 'Mamluke' OR wb_class = 'Horseman' OR wb_class = 'Scout' or wb_class = 'Arms') "
                             "AND tournament_id = :tour_id "
                             "AND map = :map "
                             "AND attacking_faction = :a_fac "
                             "AND defending_faction = :d_fac "
                             "AND team_id = attacking_team "
                             "AND teamkill = '0' ")

            result = connection.execute(query, tour_id=tournament_id, map=map, a_fac=a_fac, d_fac=d_fac)

            return DbUtils.fetch_one_value(result)

    def find_defending_faction_kills(self, tournament_id, map, a_fac, d_fac):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) AS kills "
                         "FROM Kills K "
                             "JOIN Spawns S ON K.match_id=S.match_id "
                                 "AND K.spawn=S.spawn "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                         "WHERE tournament_id = :tour_id "
                             "AND map = :map "
                             "AND attacking_faction = :a_fac "
                             "AND defending_faction = :d_fac "
                             "AND team_id = defending_team "
                             "AND teamkill = 0")

            result = connection.execute(query, tour_id=tournament_id, map=map, a_fac=a_fac, d_fac=d_fac)

            return DbUtils.fetch_one_value(result)

    def find_defending_faction_deaths(self, tournament_id, map, a_fac, d_fac):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) AS kills "
                         "FROM Kills K "
                             "JOIN Spawns S ON K.match_id=S.match_id "
                                 "AND K.spawn=S.spawn "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON K.victim_id=TTP.player_id "
                         "WHERE tournament_id = :tour_id "
                             "AND map = :map "
                             "AND attacking_faction = :a_fac "
                             "AND defending_faction = :d_fac "
                             "AND team_id = defending_team ")

            result = connection.execute(query, tour_id=tournament_id, map=map, a_fac=a_fac, d_fac=d_fac)

            return DbUtils.fetch_one_value(result)

    def find_defending_faction_melee_kills(self, tournament_id, map, a_fac, d_fac):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) AS melee_kills "
                         "FROM Kills K "
                             "JOIN Spawns S ON K.match_id=S.match_id "
                                 "AND K.spawn=S.spawn "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                             "JOIN MatchPlayers MP ON K.killer_id=MP.player_id "
                                 "AND K.match_id=MP.match_id "
                                 "AND K.spawn=MP.spawn "
                                 "AND K.round=MP.round "
                         "WHERE (wb_class = 'Sergeant' OR wb_class = 'Infantry' OR wb_class = 'Footman' OR wb_class = 'Huscarl' OR wb_class = 'Spearman') "
                         "AND tournament_id = :tour_id "
                             "AND map = :map "
                             "AND attacking_faction = :a_fac "
                             "AND defending_faction = :d_fac "
                             "AND team_id = defending_team "
                             "AND teamkill = '0' ")

            result = connection.execute(query, tour_id=tournament_id, map=map, a_fac=a_fac, d_fac=d_fac)

            return DbUtils.fetch_one_value(result)

    def find_defending_faction_ranged_kills(self, tournament_id, map, a_fac, d_fac):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) AS melee_kills "
                         "FROM Kills K "
                             "JOIN Spawns S ON K.match_id=S.match_id "
                                 "AND K.spawn=S.spawn "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                             "JOIN MatchPlayers MP ON K.killer_id=MP.player_id "
                                 "AND K.match_id=MP.match_id "
                                 "AND K.spawn=MP.spawn "
                                 "AND K.round=MP.round "
                         "WHERE (wb_class = 'Archer' OR wb_class = 'Crossbowman') "
                             "AND tournament_id = :tour_id "
                             "AND map = :map "
                             "AND attacking_faction = :a_fac "
                             "AND defending_faction = :d_fac "
                             "AND team_id = defending_team "
                             "AND teamkill = '0' ")

            result = connection.execute(query, tour_id=tournament_id, map=map, a_fac=a_fac, d_fac=d_fac)

            return DbUtils.fetch_one_value(result)

    def find_defending_faction_cav_kills(self, tournament_id, map, a_fac, d_fac):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) AS melee_kills "
                         "FROM Kills K "
                             "JOIN Spawns S ON K.match_id=S.match_id "
                                 "AND K.spawn=S.spawn "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                             "JOIN MatchPlayers MP ON K.killer_id=MP.player_id "
                                 "AND K.match_id=MP.match_id "
                                 "AND K.spawn=MP.spawn "
                                 "AND K.round=MP.round "
                         "WHERE (wb_class = 'Mamluke' OR wb_class = 'Horseman' OR wb_class = 'Scout' or wb_class = 'Arms') "
                             "AND tournament_id = :tour_id "
                             "AND map = :map "
                             "AND attacking_faction = :a_fac "
                             "AND defending_faction = :d_fac "
                             "AND team_id = defending_team "
                             "AND teamkill = '0' ")

            result = connection.execute(query, tour_id=tournament_id, map=map, a_fac=a_fac, d_fac=d_fac)

            return DbUtils.fetch_one_value(result)

    def find_open_map_wins(self, tournament_id, faction):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) as wins "
                         "FROM Spawns S "
                             "JOIN Matches M ON S.match_id=M.match_id "
                         "WHERE tournament_id = :tour_id "
                             "AND attacker_score = 3 "
                             "AND attacking_faction = :fac "
                             "AND map in ('Waterfall') "
                         "UNION ALL "
                         "SELECT count(*) "
                         "FROM Spawns S "
                             "JOIN Matches M ON S.match_id=M.match_id "
                         "WHERE tournament_id = :tour_id "
                             "AND defender_score = 3 "
                             "AND defending_faction = :fac "
                             "AND map in ('Waterfall')")

            result = connection.execute(query, tour_id=tournament_id, fac=faction)

            return result.fetchall()

    def find_mixed_map_wins(self, tournament_id, faction):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) as wins "
                         "FROM Spawns S "
                             "JOIN Matches M ON S.match_id=M.match_id "
                         "WHERE tournament_id = :tour_id "
                             "AND attacker_score = 3 "
                             "AND attacking_faction = :fac "
                             "AND map in ('Castellum', 'Frosthaven', 'Reveran Village', 'River Village', 'Naval Outpost') "
                         "UNION ALL "
                         "SELECT count(*) "
                         "FROM Spawns S "
                             "JOIN Matches M ON S.match_id=M.match_id "
                         "WHERE tournament_id = :tour_id "
                             "AND defender_score = 3 "
                             "AND defending_faction = :fac "
                             "AND map in ('Castellum', 'Frosthaven', 'Reveran Village', 'River Village', 'Naval Outpost')")

            result = connection.execute(query, tour_id=tournament_id, fac=faction)

            return result.fetchall()

    def find_closed_map_wins(self, tournament_id, faction):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) as wins "
                         "FROM Spawns S "
                             "JOIN Matches M ON S.match_id=M.match_id "
                         "WHERE tournament_id = :tour_id "
                             "AND attacker_score = 3 "
                             "AND attacking_faction = :fac "
                             "AND map in ('Beairteas', 'Fort of Honor', \"San'di'boush\", 'Verloren', 'Winterburg') "
                         "UNION ALL "
                         "SELECT count(*) "
                         "FROM Spawns S "
                             "JOIN Matches M ON S.match_id=M.match_id "
                         "WHERE tournament_id = :tour_id "
                             "AND defender_score = 3 "
                             "AND defending_faction = :fac "
                             "AND map in ('Beairteas', 'Fort of Honor', \"San'di'boush\", 'Verloren', 'Winterburg')")

            result = connection.execute(query, tour_id=tournament_id, fac=faction)

            return result.fetchall()

    def find_sets_played_on_open_maps(self, tournament_id, faction):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) "
                         "FROM Spawns S "
                             "JOIN Matches M ON S.match_id=M.match_id "
                         "WHERE tournament_id = :tour_id "
                             "AND (attacking_faction = :fac OR defending_faction = :fac) "
                             "AND map in ('Waterfall')")

            result = connection.execute(query, tour_id=tournament_id, fac=faction)

            return DbUtils.fetch_one_value(result)

    def find_sets_played_on_mixed_maps(self, tournament_id, faction):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) "
                         "FROM Spawns S "
                             "JOIN Matches M ON S.match_id=M.match_id "
                         "WHERE tournament_id = :tour_id "
                             "AND (attacking_faction = :fac OR defending_faction = :fac) "
                             "AND map in ('Castellum', 'Frosthaven', 'Reveran Village', 'River Village', 'Naval Outpost')")

            result = connection.execute(query, tour_id=tournament_id, fac=faction)

            return DbUtils.fetch_one_value(result)

    def find_sets_played_on_closed_maps(self, tournament_id, faction):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) "
                         "FROM Spawns S "
                             "JOIN Matches M ON S.match_id=M.match_id "
                         "WHERE tournament_id = :tour_id "
                             "AND (attacking_faction = :fac OR defending_faction = :fac) "
                             "AND map in ('Beairteas', 'Fort of Honor', \"San'di'boush\", 'Verloren', 'Winterburg')")

            result = connection.execute(query, tour_id=tournament_id, fac=faction)

            return DbUtils.fetch_one_value(result)

