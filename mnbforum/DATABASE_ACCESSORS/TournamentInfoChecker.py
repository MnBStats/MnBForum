from sqlalchemy.sql import text
from mnbmodel import db as DB
from .DbUtils import DbUtils

database = DB.Database('mysql+mysqlconnector://root:Buju747!@localhost/mnbstats')


class TournamentInfoChecker():
    def __init__(self):
        pass

    def find_tournaments(self):
        with database.engine.connect() as connection:
            query = text("SELECT tournament_name, tournament_id "
                         "FROM Tournaments")

            result = connection.execute(query)

        return result.fetchall()

    def find_tournament_id(self, tournament_name):
        with database.engine.connect() as connection:
            query = text("SELECT tournament_id "
                         "FROM Tournaments "
                         "WHERE tournament_name = :name")

            result = connection.execute(query, name=tournament_name)

        return DbUtils.fetch_one_value(result)

    def find_team(self, team_id):
        with database.engine.connect() as connection:
            query = text("SELECT * "
                         "FROM TournamentTeams "
                         "WHERE tournament_team_id = :id")

            result = connection.execute(query, id=str(team_id))

        return result.fetchone()

    def find_team_id(self, team_name, tournament_id):
        with database.engine.connect() as connection:
            query = text("SELECT tournament_team_id "
                         "FROM TournamentTeams "
                         "WHERE team_name = :name AND tournament_id = :id")

            result = connection.execute(query, name=team_name, id=str(tournament_id))

        return DbUtils.fetch_one_value(result)

    def find_tournament_teams(self, tournament_id):
        with database.engine.connect() as connection:
            query = text("SELECT tournament_team_id "
                         "FROM TournamentTeams "
                         "WHERE tournament_id = :id")

            result = connection.execute(query, id=str(tournament_id))

        return DbUtils.fetch_list(result)

    def find_matches_from_round(self, round_number, tournament_id):
        with database.engine.connect() as connection:
            query = text("SELECT match_id, tournament_id, home_team_id, away_team_id, home_team_score, away_team_score "
                         "FROM Matches "
                         "WHERE Matches.week_number=:week AND Matches.tournament_id=:id")

            result = connection.execute(query, week=str(round_number), id=str(tournament_id))

        return result
