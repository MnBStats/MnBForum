from sqlalchemy.sql import text
from mnbmodel import db as DB
from .DbUtils import DbUtils

database = DB.Database('mysql+mysqlconnector://root:Buju747!@localhost/mnbstats')


class FinalsStatsRetriever():
    def __init__(self, match_id):
        self.match_id = match_id

    def find_number_of_spawns(self):
        with database.engine.connect() as connection:
            query = text("SELECT count(spawn) FROM Spawns "
                         "WHERE match_id = :match_id ")

            result = connection.execute(query, match_id=self.match_id)

            return DbUtils.fetch_one_value(result)

    def find_number_of_rounds(self, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT count(distinct(round)) FROM Kills "
                         "WHERE match_id = :match_id AND spawn = :spawn ")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number)

            return DbUtils.fetch_one_value(result)

    def find_attacking_team(self, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT attacking_team "
                         "FROM Spawns S "
                         "WHERE match_id = :match_id AND spawn = :spawn")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number)

            return DbUtils.fetch_one_value(result)

    def find_defending_team(self, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT defending_team "
                         "FROM Spawns S "
                         "WHERE match_id = :match_id AND spawn = :spawn")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number)

            return DbUtils.fetch_one_value(result)

    def find_spawn_score(self, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT attacker_score, defender_score "
                         "FROM Spawns "
                         "WHERE match_id = :match_id AND spawn = :spawn")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number)

            return result.fetchall()

    def find_team_kills_for_spawn(self, team_name, spawn_number):
        with database.engine.connect() as connection:
            query = text(
                "(SELECT distinct(name) AS player_name, "
                "(SELECT count(*) as kills_count "
                    "FROM Kills KK JOIN Matches MM ON KK.match_id=MM.match_id "
                    "WHERE KK.match_id = :match_id AND "
                        "spawn = :spawn AND "
                        "team_name = :team AND "
                        "teamkill = '0' AND "
                        "KK.killer_id=K.killer_id "
                    "GROUP BY killer_id) AS kills, "
                "(SELECT count(*) AS deaths_count "
                    "FROM Kills DK JOIN Matches MM ON DK.match_id=MM.match_id "
                    "WHERE DK.match_id = :match_id AND "
                        "spawn = :spawn AND "
                        "team_name = :team AND "
                        "DK.victim_id=K.killer_id "
                    "GROUP BY DK.victim_id) AS deaths, "
                "(SELECT count(*) AS teamkills_count "
                    "FROM Kills TK JOIN Matches MM ON TK.match_id=MM.match_id "
                    "WHERE TK.match_id = :match_id AND "
                        "spawn = :spawn AND "
                        "team_name = :team AND "
                        "teamkill = '1' AND "
                        "TK.killer_id=K.killer_id) AS teamkills "
                "FROM Kills K "
                "JOIN Players P ON K.killer_id=P.player_id "
                "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                "JOIN Matches M ON K.match_id=M.match_id "
                "JOIN TournamentTeams TT ON TTP.team_id=TT.tournament_team_id "
                    "AND TT.tournament_id=M.tournament_id "
                "WHERE K.match_id = :match_id AND "
                    "spawn = :spawn AND "
                    "team_name = :team "
                "GROUP BY player_name) "

                "UNION ALL "

                "(SELECT distinct(name) AS player_name, "
                "(SELECT count(*) AS kills_count "
                    "FROM Kills KK JOIN Matches MM ON KK.match_id=MM.match_id "
                    "WHERE KK.match_id = :match_id AND "
                        "spawn = :spawn AND "
                        "team_name = :team AND "
                        "teamkill = '0' AND "
                        "KK.killer_id=K.victim_id "
                    "GROUP BY KK.killer_id) AS kills, "
                "(SELECT count(*) as deaths_count "
                    "FROM Kills DK JOIN Matches MM ON DK.match_id=MM.match_id "
                    "WHERE DK.match_id = :match_id AND "
                        "spawn = :spawn AND "
                        "team_name = :team AND "
                        "DK.victim_id=K.victim_id "
                    "GROUP BY DK.victim_id) AS deaths, "
                "(SELECT count(*) AS teamkills_count "
                    "FROM Kills TK JOIN Matches MM ON TK.match_id=MM.match_id "
                    "WHERE TK.match_id = :match_id AND "
                        "spawn = :spawn AND "
                        "team_name = :team AND "
                        "teamkill = '1' AND "
                        "TK.killer_id=K.victim_id "
                    "GROUP BY TK.killer_id) AS teamkills "
                "FROM Kills K "
                "JOIN Players P ON K.victim_id=P.player_id "
                "JOIN TournamentTeamPlayers TTP ON K.victim_id=TTP.player_id "
                "JOIN Matches M ON K.match_id=M.match_id "
                "JOIN TournamentTeams TT ON TTP.team_id=TT.tournament_team_id "
                    "AND TT.tournament_id=M.tournament_id "
                "WHERE K.match_id = :match_id AND "
                    "spawn = :spawn AND "
                    "team_name = :team "
                "GROUP BY player_name "
                "HAVING kills is NULL AND teamkills is NULL ) "
                "ORDER BY kills DESC, deaths ASC")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number, team=team_name)

            return DbUtils.fetch_without_nulls(result)

    def find_team_assists_for_spawn(self, team_name, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name), sum(number_of_assists) AS assists "
                         "FROM Assists A "
                             "JOIN Players P ON A.player_id=P.player_id "
                             "JOIN TournamentTeamPlayers TTP ON A.player_id=TTP.player_id "
                             "JOIN Matches M ON A.match_id=M.match_id "
                             "JOIN TournamentTeams TT ON TTP.team_id=TT.tournament_team_id "
                                 "AND TT.tournament_id=M.tournament_id "
                         "WHERE A.match_id = :match_id AND spawn = :spawn AND team_name = :team "
                         "GROUP BY name ORDER BY assists DESC")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number, team=team_name)

            return result.fetchall()

    def find_team_classes_for_spawn(self, team_name, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT name, wb_class AS class "
                         "FROM MatchPlayers MP "
                             "JOIN Players P ON MP.player_id=P.player_id "
                             "JOIN TournamentTeamPlayers TTP ON MP.player_id=TTP.player_id "
                             "JOIN Matches M ON MP.match_id=M.match_id "
                             "JOIN TournamentTeams TT ON TTP.team_id=TT.tournament_team_id "
                                 "AND TT.tournament_id=M.tournament_id "
                         "WHERE MP.match_id = :match_id AND spawn = :spawn AND team_name = :team "
                         "GROUP BY name, class ORDER BY name DESC")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number, team=team_name)

            return result.fetchall()

    def find_team_damage_for_spawn(self, team_name, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, sum(dmg) AS damage "
                         "FROM Assists A "
                             "JOIN Players P ON A.player_id=P.player_id "
                             "JOIN TournamentTeamPlayers TTP ON A.player_id=TTP.player_id "
                             "JOIN Matches M ON A.match_id=M.match_id "
                             "JOIN TournamentTeams TT ON TTP.team_id=TT.tournament_team_id "
                                 "AND TT.tournament_id=M.tournament_id "
                         "WHERE A.match_id = :match_id "
                             "AND spawn = :spawn "
                             "AND team_name = :team "
                         "GROUP BY player_name ORDER BY damage DESC")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number, team=team_name)

            return result.fetchall()

    def find_team_total_kills_for_spawn(self, team_id, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) "
                         "FROM Kills K "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                         "WHERE K.match_id = :match_id "
                             "AND spawn = :spawn "
                             "AND teamkill = '0' "
                             "AND team_id = :team")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number, team=team_id)

            return DbUtils.fetch_one_value(result)


    def find_team_total_deaths_for_spawn(self, team_id, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) "
                         "FROM Kills K "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON K.victim_id=TTP.player_id "
                         "WHERE K.match_id = :match_id "
                             "AND spawn = :spawn "
                             "AND team_id = :team")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number, team=team_id)

            return DbUtils.fetch_one_value(result)

    def find_team_melee_kills_for_spawn(self, team_id, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name), count(*) AS melee_kills "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                         "WHERE (kill_type='<img=ico_swordone>' OR kill_type='<img=ico_axeone>') "
                             "AND match_id = :match_id "
                             "AND spawn = :spawn "
                             "AND team_id = :team "
                             "AND teamkill = '0' "
                         "GROUP BY name ORDER BY melee_kills DESC")

            result=connection.execute(query, match_id=self.match_id, spawn=spawn_number, team=team_id)

            return result.fetchall()

    def find_team_couched_lances_for_spawn(self, team_id, spawn_number):
         with database.engine.connect() as connection:
             query = text("SELECT distinct(name), count(*) AS couched_lances "
                          "FROM Kills K "
                              "JOIN Players P ON K.killer_id=P.player_id "
                              "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                          "WHERE kill_type='<img=ico_couchedlance>' "
                              "AND match_id = :match_id "
                              "AND spawn = :spawn "
                              "AND team_id = :team "
                              "AND teamkill = '0' "
                          "GROUP BY name ORDER BY couched_lances DESC")

             result = connection.execute(query, match_id=self.match_id, spawn=spawn_number, team=team_id)

             return result.fetchall()

    def find_team_headshots_for_spawn(self, team_id, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name), count(*) AS headshots "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                         "WHERE kill_type='<img=ico_headshot>' "
                             "AND match_id = :match_id "
                             "AND spawn = :spawn "
                             "AND team_id = :team "
                             "AND teamkill = '0' "
                         "GROUP BY name ORDER BY headshots DESC")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number, team=team_id)

            return result.fetchall()

    def find_team_kill_streaks_for_spawn(self, team_id, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT name, round "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN TournamentTeamPlayers TTP ON P.player_id=TTP.player_id "
                         "WHERE match_id = :match_id "
                             "AND spawn = :spawn "
                             "AND team_id = :team "
                             "AND teamkill = '0'")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number, team=team_id)

            return result.fetchall()

    def find_team_total_melee_kills_for_spawn(self, team_id, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) AS melee_kills "
                         "FROM Kills K "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                             "JOIN MatchPlayers MP ON K.killer_id=MP.player_id "
                                 "AND K.match_id=MP.match_id "
                                 "AND K.spawn=MP.spawn "
                                 "AND K.round=MP.round "
                         "WHERE (wb_class = 'Sergeant' OR wb_class = 'Infantry' OR wb_class = 'Footman' OR wb_class = 'Huscarl' OR wb_class = 'Spearman') "
                         "AND K.match_id = :match_id "
                             "AND K.spawn = :spawn "
                             "AND team_id = :team "
                         "AND teamkill = '0' ")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number, team=team_id)

            return DbUtils.fetch_one_value(result)

    def find_team_total_ranged_kills_for_spawn(self, team_id, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) AS melee_kills "
                         "FROM Kills K "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                             "JOIN MatchPlayers MP ON K.killer_id=MP.player_id "
                                 "AND K.match_id=MP.match_id "
                                 "AND K.spawn=MP.spawn "
                                 "AND K.round=MP.round "
                         "WHERE (wb_class = 'Archer' OR wb_class = 'Crossbowman') "
                             "AND K.match_id = :match_id "
                             "AND K.spawn = :spawn "
                             "AND team_id = :team "
                         "AND teamkill = '0' ")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number, team=team_id)

            return DbUtils.fetch_one_value(result)

    def find_team_total_cav_kills_for_spawn(self, team_id, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) AS melee_kills "
                         "FROM Kills K "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                             "JOIN MatchPlayers MP ON K.killer_id=MP.player_id "
                                 "AND K.match_id=MP.match_id "
                                 "AND K.spawn=MP.spawn "
                                 "AND K.round=MP.round "
                         "WHERE (wb_class = 'Mamluke' OR wb_class = 'Horseman' OR wb_class = 'Scout' or wb_class = 'Arms') "
                             "AND K.match_id = :match_id "
                             "AND K.spawn = :spawn "
                             "AND team_id = :team "
                             "AND teamkill = '0' ")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number, team=team_id)

            return DbUtils.fetch_one_value(result)

    def find_team_teamkills_for_round(self, team_id, spawn_number, round_number):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) AS team_kills "
                         "FROM Kills K "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                         "WHERE match_id = :match_id "
                             "AND spawn = :spawn "
                             "AND round = :round "
                             "AND team_id = :team "
                             "AND teamkill = '1' ")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number, round=round_number, team=team_id)

            return DbUtils.fetch_one_value(result)

    def find_first_kill_for_round(self, spawn_number, round_number):
        with database.engine.connect() as connection:
            query = text("SELECT team_id, teamkill "
                         "FROM Kills K "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                         "WHERE K.match_id = :match_id "
                             "AND spawn = :spawn "
                             "AND round = :round "
                             "AND kill_number = 1 ")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number, round=round_number)

            return result.fetchall()

    def find_team_kills_for_round(self, spawn_number, round_number):
        with database.engine.connect() as connection:
            query = text("SELECT team_id, kill_number "
                         "FROM Kills K "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                         "WHERE match_id = :match_id "
                             "AND spawn = :spawn "
                             "AND round = :round "
                             "AND teamkill = '0' "
                         "ORDER BY kill_number ASC")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number, round=round_number)

            return result.fetchall()

    def find_team_kill_types_for_spawn(self, team_id, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(kill_type) AS kill_type, count(*) AS kills "
                         "FROM Kills K "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                         "WHERE match_id = :match_id "
                             "AND spawn = :spawn "
                             "AND team_id = :team "
                             "AND teamkill = '0' "
                         "GROUP BY kill_type ORDER BY kills DESC")

            result = connection.execute(query, match_id=self.match_id, spawn=spawn_number, team=team_id)

            return result.fetchall()
