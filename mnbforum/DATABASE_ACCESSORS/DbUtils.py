class DbUtils:
    def __init__(self):
        pass

    @staticmethod
    def fetch_one_value(cursor):
        row = cursor.fetchone()
        if row is None:
            return None

        return row[0]

    @staticmethod
    def fetch_list(cursor):
        l = list()
        rows = cursor.fetchall()
        for row in rows:
            l.append(row[0])

        return l

    @staticmethod
    def fetch_without_nulls(cursor):
        data = cursor.fetchall()

        result = []
        for row in data:
            ls = [0 if str(field) == "None" else field for field in row]
            result.append(tuple(ls))

        return result

