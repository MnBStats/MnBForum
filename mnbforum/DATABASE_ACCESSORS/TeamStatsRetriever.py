from sqlalchemy.sql import text
from mnbmodel import db as DB
from .DbUtils import DbUtils

database = DB.Database('mysql+mysqlconnector://root:Buju747!@localhost/mnbstats')


class TeamStatsRetriever:
    def __init__(self, team_id):
        self.team_id = team_id

    def find_number_of_matches(self):
        with database.engine.connect() as connection:
            query = text("SELECT count(distinct(match_id)) "
                         "FROM Matches "
                         "WHERE home_team_id = :team OR away_team_id = :team ")

            result = connection.execute(query, team=self.team_id)

            return DbUtils.fetch_one_value(result)

    def find_total_kills(self):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) "
                         "FROM Kills K "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                         "WHERE team_id = :team "
                             "AND teamkill = '0' ")

            result = connection.execute(query, team=self.team_id)

            return DbUtils.fetch_one_value(result)

    def find_total_deaths(self):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) "
                         "FROM Kills K "
                             "JOIN TournamentTeamPlayers TTP ON K.victim_id=TTP.player_id "
                         "WHERE team_id = :team ")

            result = connection.execute(query, team=self.team_id)

            return DbUtils.fetch_one_value(result)

    def find_kill_types(self):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(kill_type) AS kill_type, count(*) AS kills "
                         "FROM Kills K "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                         "WHERE team_id = :team "
                             "AND teamkill = '0' "
                         "GROUP BY kill_type ORDER BY kills DESC ")

            result = connection.execute(query, team=self.team_id)

            return result.fetchall()

    def find_spawn_info(self):
        with database.engine.connect() as connection:
            query = text("SELECT match_id, spawn, map, "
                             "attacking_team, attacking_faction, defending_team, defending_faction "
                         "FROM Spawns "
                         "WHERE attacking_team = :team OR defending_team = :team ")

            result = connection.execute(query, team=self.team_id)

            return result.fetchall()

    def find_kills_for_spawn(self, match_id, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) "
                         "FROM Kills K "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                         "WHERE K.match_id = :match_id "
                             "AND spawn = :spawn "
                             "AND teamkill = '0' "
                             "AND team_id = :team")

            result = connection.execute(query, match_id=match_id, spawn=spawn_number, team=self.team_id)

            return DbUtils.fetch_one_value(result)


    def find_deaths_for_spawn(self, match_id, spawn_number):
        with database.engine.connect() as connection:
            query = text("SELECT count(*) "
                         "FROM Kills K "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON K.victim_id=TTP.player_id "
                         "WHERE K.match_id = :match_id "
                             "AND spawn = :spawn "
                             "AND team_id = :team")

            result = connection.execute(query, match_id=match_id, spawn=spawn_number, team=self.team_id)

            return DbUtils.fetch_one_value(result)

    def find_match_scores(self):
        with database.engine.connect() as connection:
            query = text("SELECT home_team_id, home_team_score, away_team_id, away_team_score "
                         "FROM Matches "
                         "WHERE home_team_id = :team OR away_team_id = :team")

            result = connection.execute(query, team=self.team_id)

            return result.fetchall()

