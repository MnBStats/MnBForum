from sqlalchemy.sql import text
from mnbmodel import db as DB
from .DbUtils import DbUtils

database = DB.Database('mysql+mysqlconnector://root:Buju747!@localhost/mnbstats')


class MatchStatsRetriever():
    def __init__(self):
        pass

    def find_kills_for_team(self, tournament_id, match_id, team):
        with database.engine.connect() as connection:
            query = text(
                "(SELECT distinct(name) AS player_name, "
                "(SELECT count(*) AS kill_count "
                    "FROM Kills KK JOIN Matches MM ON KK.match_id=MM.match_id "
                    "WHERE tournament_id = :tour_id AND "
                        "KK.match_id = :match_id AND "
                        "team_name = :team AND "
                        "KK.teamkill = 0 AND "
                        "KK.killer_id=K.killer_id "
                    "GROUP BY KK.killer_id) AS kills, "
                "(SELECT count(*) AS death_count "
                    "FROM Kills DK JOIN Matches MM ON DK.match_id=MM.match_id "
                    "WHERE tournament_id = :tour_id AND "
                        "DK.match_id = :match_id AND "
                        "team_name = :team AND "
                        "DK.victim_id=K.killer_id "
                    "GROUP BY DK.victim_id) AS deaths, "
                "(SELECT count(*) AS teamkill_count "
                    "FROM Kills TK JOIN Matches MM ON TK.match_id=MM.match_id "
                    "WHERE tournament_id = :tour_id AND "
                        "TK.match_id = :match_id AND "
                        "team_name = :team AND "
                        "TK.teamkill = 1 AND "
                        "TK.killer_id=K.killer_id) AS teamkills "
                "FROM Kills K "
                    "JOIN Players P ON K.killer_id=P.player_id "
                    "JOIN TournamentTeamPlayers TTP ON K.killer_id=TTP.player_id "
                    "JOIN Matches M ON K.match_id=M.match_id "
                    "JOIN TournamentTeams TT ON TTP.team_id=TT.tournament_team_id "
                        "AND TT.tournament_id=M.tournament_id "
                "WHERE M.tournament_id = :tour_id "
                    "AND K.match_id = :match_id "
                    "AND team_name = :team "
                "GROUP BY player_name) "

                "UNION ALL "

                "(SELECT distinct(name) AS player_name, "
                "(SELECT count(*) AS kill_count "
                    "FROM Kills KK JOIN Matches MM ON KK.match_id=MM.match_id "
                    "WHERE tournament_id = :tour_id AND "
                        "KK.match_id = :match_id AND "
                        "team_name = :team AND "
                        "KK.teamkill = 0 AND "
                        "KK.killer_id=K.victim_id "
                    "GROUP BY KK.killer_id) AS kills, "
                "(SELECT count(*) as death_count "
                    "FROM Kills DK JOIN Matches MM ON DK.match_id=MM.match_id "
                    "WHERE tournament_id = :tour_id AND "
                        "DK.match_id = :match_id AND "
                        "team_name = :team AND "
                        "DK.victim_id=K.victim_id "
                    "GROUP BY DK.victim_id) AS deaths, "
                "(SELECT count(*) AS teamkill_count "
                    "FROM Kills TK JOIN Matches MM ON TK.match_id=MM.match_id "
                    "WHERE tournament_id = :tour_id AND "
                        "TK.match_id = :match_id AND "
                        "team_name = :team AND "
                        "TK.teamkill = 1 AND "
                        "TK.killer_id=K.victim_id "
                    "GROUP BY TK.killer_id) AS teamkills "
                "FROM Kills K "
                    "JOIN Players P ON K.victim_id=P.player_id "
                    "JOIN TournamentTeamPlayers TTP ON K.victim_id=TTP.player_id "
                    "JOIN Matches M ON K.match_id=M.match_id "
                    "JOIN TournamentTeams TT ON TTP.team_id=TT.tournament_team_id "
                        "AND TT.tournament_id=M.tournament_id "
                "WHERE M.tournament_id = :tour_id AND "
                    "K.match_id = :match_id AND "
                    "team_name = :team "
                "GROUP BY player_name HAVING kills is NULL AND teamkills is NULL ) "
                "ORDER BY kills DESC, deaths ASC")

            result = connection.execute(query, tour_id=tournament_id, match_id=match_id, team=team)

            return DbUtils.fetch_without_nulls(result)

    def find_assists_for_team(self, tournament_id, match_id, team):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name), sum(number_of_assists) AS assists "
                         "FROM Assists A "
                             "JOIN Players P ON A.player_id=P.player_id "
                             "JOIN TournamentTeamPlayers TTP ON A.player_id=TTP.player_id "
                             "JOIN Matches M ON A.match_id=M.match_id "
                             "JOIN TournamentTeams TT ON TTP.team_id=TT.tournament_team_id "
                                 "AND TT.tournament_id=M.tournament_id "
                         "WHERE M.tournament_id = :tour_id "
                             "AND A.match_id = :match_id AND "
                             "team_name = :team "
                         "GROUP BY name ORDER BY assists DESC")

            result = connection.execute(query, tour_id=tournament_id, match_id=match_id, team=team)

            return result.fetchall()

    def find_classes_for_team(self, tournament_id, match_id, team):
        with database.engine.connect() as connection:
            query = text("SELECT name, wb_class AS class "
                         "FROM MatchPlayers MP "
                             "JOIN Players P ON MP.player_id=P.player_id "
                             "JOIN TournamentTeamPlayers TTP ON MP.player_id=TTP.player_id "
                             "JOIN Matches M ON MP.match_id=M.match_id "
                             "JOIN TournamentTeams TT ON TTP.team_id=TT.tournament_team_id "
                                 "AND TT.tournament_id=M.tournament_id "
                         "WHERE M.tournament_id = :tour_id "
                             "AND MP.match_id = :match_id "
                             "AND team_name = :team "
                         "GROUP BY name, class ORDER BY name DESC")

            result = connection.execute(query, tour_id=tournament_id, match_id=match_id, team=team)

            return result.fetchall()

    def find_damage_for_team(self, tournament_id, match_id, team):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, sum(dmg) AS damage "
                         "FROM Assists A "
                             "JOIN Players P ON A.player_id=P.player_id "
                             "JOIN TournamentTeamPlayers TTP ON A.player_id=TTP.player_id "
                             "JOIN Matches M ON A.match_id=M.match_id "
                             "JOIN TournamentTeams TT ON TTP.team_id=TT.tournament_team_id "
                                 "AND TT.tournament_id=M.tournament_id "
                         "WHERE M.tournament_id = :tour_id "
                             "AND A.match_id = :match_id "
                             "AND team_name = :team "
                         "GROUP BY player_name ORDER BY damage DESC")

            result = connection.execute(query, tour_id=tournament_id, match_id=match_id, team=team)

            return result.fetchall()

    def find_sets_played_for_team(self, tournament_id, match_id, team):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name), count(DISTINCT MP.match_id, spawn) AS sets_played "
                         "FROM MatchPlayers MP "
                             "JOIN Players P ON MP.player_id=P.player_id "
                             "JOIN TournamentTeamPlayers TTP ON MP.player_id=TTP.player_id "
                             "JOIN Matches M ON MP.match_id=M.match_id "
                             "JOIN TournamentTeams TT ON TTP.team_id=TT.tournament_team_id "
                                 "AND TT.tournament_id=M.tournament_id "
                         "WHERE M.tournament_id = :tour_id "
                             "AND MP.match_id = :match_id "
                             "AND team_name = :team "
                         "GROUP BY name ORDER BY sets_played DESC")

            result = connection.execute(query, tour_id=tournament_id, match_id=match_id, team=team)

            return result.fetchall()

    def find_headshots(self, tournament_id, week, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) as player_name, count(*) as headshots "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                         "WHERE kill_type='<img=ico_headshot>' "
                             "AND tournament_id = :tour_id "
                             "AND week_number = :week "
                             "AND teamkill = 0 "
                         "GROUP BY player_name ORDER BY headshots DESC, player_name DESC LIMIT :limit")

            result = connection.execute(query, tour_id=tournament_id, week=week, limit=limit)

            return result.fetchall()

    def find_couched_lances(self, tournament_id, week, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) as player_name, count(*) as couched_lances "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                         "WHERE kill_type='<img=ico_couchedlance>' "
                             "AND tournament_id= :tour_id "
                             "AND week_number = :week "
                             "AND teamkill = 0 "
                         "GROUP BY player_name ORDER BY couched_lances DESC, player_name DESC LIMIT :limit")

            result = connection.execute(query, tour_id=tournament_id, week=week, limit=limit)

            return result.fetchall()

    def find_melee_kills(self, tournament_id, week, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) as player_name, count(*) as melee_kills "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                         "WHERE (kill_type='<img=ico_swordone>' OR kill_type='<img=ico_axeone>') "
                             "AND tournament_id = :tour_id "
                             "AND week_number = :week "
                             "AND teamkill = 0 "
                         "GROUP BY player_name ORDER BY melee_kills DESC, player_name DESC LIMIT :limit")

            result = connection.execute(query, tour_id=tournament_id, week=week, limit=limit)

            return result.fetchall()

    def find_club_20(self, tournament_id, week):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) as player_name, count(*) as player_kills "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                         "WHERE tournament_id = :tour_id "
                             "AND week_number = :week "
                             "AND teamkill = 0 "
                         "GROUP BY player_name HAVING player_kills >= 20 ORDER BY player_kills DESC")

            result = connection.execute(query, tour_id=tournament_id, week=week)

            return result.fetchall()

    def find_star_of_the_week(self, tournament_id, week):
        with database.engine.connect() as connection:
            query = text("SELECT name AS player_name, count(*) AS kills "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                         "WHERE tournament_id = :tour_id "
                             "AND week_number = :week "
                             "AND teamkill = 0 "
                         "GROUP BY player_name ORDER BY kills DESC, player_name DESC LIMIT 1")

            result = connection.execute(query, tour_id=tournament_id, week=week)
            return result.fetchone()

    def find_headshots_congrats(self, tournament_id, week, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) as player_name, count(*) as headshots "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE kill_type='<img=ico_headshot>' "
                             "AND M.tournament_id = :tour_id "
                             "AND week_number <= :week "
                             "AND team_name in ({}) "
                             "AND teamkill = 0 "
                         "GROUP BY player_name ORDER BY headshots DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, week=week, **team_list, limit=limit)

            return result.fetchall()

    def find_couched_lances_congrats(self, tournament_id, week, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) as player_name, count(*) as couched_lances "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE kill_type='<img=ico_couchedlance>' "
                             "AND M.tournament_id = :tour_id "
                             "AND week_number <= :week "
                             "AND team_name IN ({}) "
                             "AND teamkill = 0 "
                         "GROUP BY player_name ORDER BY couched_lances DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, week=week, **team_list, limit=limit)

            return result.fetchall()

    def find_melee_kills_congrats(self, tournament_id, week, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) as player_name, count(*) as melee_kills "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE (kill_type='<img=ico_swordone>' OR kill_type='<img=ico_axeone>') "
                             "AND M.tournament_id = :tour_id "
                             "AND week_number <= :week "
                             "AND team_name IN ({}) "
                             "AND teamkill = 0 "
                         "GROUP BY player_name ORDER BY melee_kills DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, week=week, **team_list, limit=limit)

            return result.fetchall()

