from sqlalchemy.sql import text
from mnbmodel import db as DB
from .DbUtils import DbUtils

database = DB.Database('mysql+mysqlconnector://root:Buju747!@localhost/mnbstats')


class PlayerStatsRetriever():
    def __init__(self):
        pass

    def find_player_stats(self, tournament_id, team_list):
        with database.engine.connect() as connection:
            query = text(
                "(SELECT distinct(name) AS player_name, "
                "(SELECT count(*) AS kill_count "
                    "FROM Kills KK JOIN Matches MM ON KK.match_id=MM.match_id "
                    "WHERE MM.tournament_id = :tour_id "
                        "AND team_name IN ({0}) "
                        "AND KK.teamkill = 0 "
                        "AND KK.killer_id=K.killer_id "
                    "GROUP BY KK.killer_id) AS kills, "
                "(SELECT count(*) AS death_count "
                    "FROM Kills DK JOIN Matches DM ON DK.match_id=DM.match_id "
                    "WHERE DM.tournament_id = :tour_id "
                        "AND team_name IN ({0}) "
                        "AND DK.victim_id=K.killer_id "
                    "GROUP BY DK.victim_id) AS deaths, "
                "(SELECT count(*) AS teamkill_count "
                    "FROM Kills TK JOIN Matches TM ON TK.match_id=TM.match_id "
                    "WHERE TM.tournament_id = :tour_id "
                        "AND team_name IN ({0}) "
                        "AND TK.teamkill = 1 "
                        "AND TK.killer_id=K.killer_id) AS teamkills, "
                "count(distinct(M.match_id)) AS matches_played "
                "FROM Kills K "
                    "JOIN Players P ON K.killer_id=P.player_id "
                    "JOIN Matches M on K.match_id=M.match_id "
                    "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                    "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                        "AND M.tournament_id=TT.tournament_id "
                "WHERE M.tournament_id = :tour_id  "
                    "AND team_name IN ({0}) "
                "GROUP BY player_name) "

                "UNION ALL "

                "(SELECT distinct(name) AS player_name, "
                "(SELECT count(*) AS kill_count "
                    "FROM Kills KK JOIN Matches MM ON KK.match_id=MM.match_id "
                    "WHERE MM.tournament_id = :tour_id "
                        "AND team_name IN ({0}) "
                        "AND KK.teamkill = 0 "
                        "AND KK.killer_id=K.victim_id "
                    "GROUP BY KK.killer_id) AS kills, "
                "(SELECT count(*) AS death_count "
                    "FROM Kills DK JOIN Matches DM ON DK.match_id=DM.match_id "
                    "WHERE DM.tournament_id = :tour_id "
                        "AND team_name IN ({0}) "
                        "AND DK.victim_id=K.victim_id "
                    "GROUP BY DK.victim_id) AS deaths, "
                "(SELECT count(*) AS teamkill_count "
                    "FROM Kills TK JOIN Matches TM ON TK.match_id=TM.match_id "
                    "WHERE TM.tournament_id = :tour_id "
                        "AND team_name IN ({0}) "
                        "AND TK.teamkill = 1 "
                        "AND TK.killer_id=K.victim_id "
                    "GROUP BY TK.killer_id) AS teamkills, "
                "count(distinct(M.match_id)) AS matches_played "
                "FROM Kills K "
                    "JOIN Players P ON K.victim_id=P.player_id "
                    "JOIN Matches M ON K.match_id=M.match_id "
                    "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                    "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                         "AND M.tournament_id=TT.tournament_id "
                "WHERE M.tournament_id = :tour_id "
                    "AND team_name IN ({0}) "
                "GROUP BY player_name HAVING kills IS NULL AND teamkills IS NULL) "
                "ORDER BY kills DESC, deaths ASC".format(', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list)

            return DbUtils.fetch_without_nulls(result)

    def find_assists(self, tournament_id, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT name, sum(number_of_assists) AS assists "
                         "FROM Assists A "
                             "JOIN Matches M ON A.match_id=M.match_id "
                             "JOIN Players P ON A.player_id=P.player_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                         "GROUP BY name ORDER BY assists DESC, name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list, limit=limit)

            return result.fetchall()

    def find_headshots(self, tournament_id, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, count(*) AS headshots "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE kill_type='<img=ico_headshot>' "
                             "AND M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                             "AND teamkill = 0 "
                         "GROUP BY player_name ORDER BY headshots DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list, limit=limit)

            return result.fetchall()

    def find_couched_lances(self, tournament_id, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, count(*) AS couched_lances "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE kill_type='<img=ico_couchedlance>' "
                             "AND M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                             "AND teamkill = 0 "
                         "GROUP BY player_name ORDER BY couched_lances DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list, limit=limit)

            return result.fetchall()

    def find_melee_kills(self, tournament_id, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, count(*) AS melee_kills "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE (kill_type='<img=ico_swordone>' OR kill_type='<img=ico_axeone>') "
                             "AND M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                             "AND teamkill = 0 "
                         "GROUP BY player_name ORDER BY melee_kills DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list, limit=limit)

            return result.fetchall()

    def find_club_20(self, tournament_id, team_list):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, count(*) AS player_kills "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M On K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                             "AND teamkill = 0 "
                         "GROUP BY player_name, M.match_id HAVING player_kills >= 20 ORDER BY player_kills DESC".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list)

            return result.fetchall()

    def find_hammer_kills(self, tournament_id, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, count(*) AS hammer_kills "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE kill_type='<img=ico_maul>' "
                             "AND M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                             "AND teamkill = 0 "
                         "GROUP BY player_name ORDER BY hammer_kills DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list, limit=limit)

            return result.fetchall()

    def find_2h_kills(self, tournament_id, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, count(*) AS 2h_kills "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE " # (kill_type='<img=ico_swordtwo>' OR kill_type='<img=ico_axetwo>') OR "
                             "weapon_id IN ('Axe', 'Bardiche', 'Battle Axe', 'Great Axe', 'Great Bardiche', "
                               "'Great Sword', 'Great Hammer', 'Iron Mace', 'Maul', 'Sarranid Battle Axe', 'Long War Axe', "
                               "'Sarranid War Axe', 'Shortened Military Scythe', 'Shortened Voulge', 'Sledge Hammer', "
                               "'Two Handed Axe', 'Two Handed Sword', 'Two Handed War Axe', 'War Axe', 'War Cleaver') "
                             "AND M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                             "AND teamkill = 0 "
                         "GROUP BY player_name ORDER BY 2h_kills DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list, limit=limit)

            return result.fetchall()

    def find_teamkills(self, tournament_id, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, count(*) AS team_kills "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE teamkill = 1 "
                             "AND M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                         "GROUP BY player_name ORDER BY team_kills DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list, limit=limit)

            return result.fetchall()

    def find_teamkilled(self, tournament_id, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, count(*) AS team_kills "
                         "FROM Kills K "
                             "JOIN Players P ON K.victim_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE teamkill = 1 "
                             "AND M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                         "GROUP BY player_name ORDER BY team_kills DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list, limit=limit)

            return result.fetchall()

    def find_first_deaths(self, tournament_id, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, count(*) AS first_deaths "
                         "FROM Kills K "
                             "JOIN Players P ON K.victim_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE kill_number = 1 "
                             "AND M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                         "GROUP BY player_name ORDER BY first_deaths DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list, limit=limit)

            return result.fetchall()

    def find_first_kills(self, tournament_id, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, count(*) AS first_kills "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE kill_number = 1 "
                             "AND M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                         "GROUP BY player_name ORDER BY first_kills DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list, limit=limit)

            return result.fetchall()

    def find_last_survivors(self, tournament_id, team_list):
        with database.engine.connect() as connection:
            query = text("SELECT MP.match_id, spawn, round, name AS player_name, side-1 "
                         "FROM MatchPlayers MP "
                             "JOIN Players P ON MP.player_id=P.player_id "
                             "JOIN Matches M ON MP.match_id=M.match_id "
                             "JOIN TournamentTeams HTT ON M.home_team_id=HTT.tournament_team_id "
                             "JOIN TournamentTeams ATT ON M.away_team_id=ATT.tournament_team_id "
                         "WHERE M.tournament_id = :tour_id "
                             "AND (HTT.team_name IN ({0}) OR ATT.team_name IN ({0})) "
                         "UNION ALL "
                         "SELECT K.match_id, spawn, round, name, kill_number "
                         "FROM Kills K "
                             "JOIN Players P ON K.victim_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeams HTT ON M.home_team_id=HTT.tournament_team_id "
                             "JOIN TournamentTeams ATT ON M.away_team_id=ATT.tournament_team_id "
                         "WHERE M.tournament_id = :tour_id "
                             "AND (HTT.team_name IN ({0}) OR ATT.team_name IN ({0}))".format(
                                 ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list)

            return result.fetchall()

    def find_morningstar_kills(self, tournament_id, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, count(*) AS morningstar_kills "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE kill_type='<img=ico_morningstar>' "
                             "AND M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                             "AND teamkill = 0 "
                         "GROUP BY player_name ORDER BY morningstar_kills DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list, limit=limit)

            return result.fetchall()

    def find_spear_kills(self, tournament_id, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, count(*) AS spear_kills "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M On K.match_id=M.match_id "
                             "JOIN MatchPlayers MP ON K.match_id=MP.match_id "
                                "AND K.spawn=MP.spawn "
                                "AND K.round=MP.round "
                                "AND K.killer_id=MP.player_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE kill_type='<img=ico_spear>' "
                             "AND wb_class IN ('Footman', 'Infantry', 'Spearman', 'Sergeant', 'Huscarl') "
                             "AND M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                             "AND teamkill = 0 "
                         "GROUP BY player_name ORDER BY spear_kills DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list, limit=limit)

            return result.fetchall()

    def find_throwing_weapon_kills(self, tournament_id, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, count(*) AS throwing_kills "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE weapon_id IN ('None-weapon', 'Javelins', 'Jarids', 'Throwing Spears', 'Darts', 'War Darts', 'Light Throwing Axes', 'Throwing Axes', 'Heavy Throwing Axes') "
                             "AND M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                             "AND teamkill = 0 "
                         "GROUP BY player_name ORDER BY throwing_kills DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list, limit=limit)

            return result.fetchall()

    def find_damage(self, tournament_id, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, sum(dmg) AS damage "
                         "FROM Assists A "
                             "JOIN Players P ON A.player_id=P.player_id "
                             "JOIN Matches M ON A.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                         "GROUP BY player_name ORDER BY damage DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list, limit=limit)

            return result.fetchall()

    def find_unarmed_kills(self, tournament_id, team_list, limit):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, count(*) AS unarmed_kills "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE kill_type='<img=ico_punch>' "
                             "AND M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                             "AND teamkill = 0 "
                         "GROUP BY player_name ORDER BY unarmed_kills DESC, player_name DESC LIMIT :limit".format(
                             ', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list, limit=limit)

            return result.fetchall()

    def find_kill_streaks(self, tournament_id, team_list):
        with database.engine.connect() as connection:
            query = text("SELECT name, K.match_id, spawn, round "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE M.tournament_id = :tour_id "
                             "AND team_name IN ({}) "
                             "AND teamkill = 0".format(', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list)

            return result.fetchall()

    def find_kd_ratio(self, tournament_id, team_list):
        with database.engine.connect() as connection:
            query = text("SELECT distinct(name) AS player_name, "
                         "(SELECT count(*) as kill_count "
                             "FROM Kills KK JOIN Matches MM ON KK.match_id=MM.match_id "
                             "WHERE MM.tournament_id = :tour_id "
                                 "AND team_name IN ({0}) "
                                 "AND KK.teamkill = 0 "
                                 "AND KK.killer_id=K.killer_id "
                             "GROUP BY KK.killer_id) AS kills, "
                         "(SELECT count(*) AS death_count "
                             "FROM Kills DK JOIN Matches DM ON DK.match_id=DM.match_id "
                             "WHERE DM.tournament_id = :tour_id "
                                 "AND team_name IN ({0}) "
                                 "AND DK.victim_id=K.killer_id "
                             "GROUP BY DK.victim_id) AS deaths "
                         "FROM Kills K "
                             "JOIN Players P ON K.killer_id=P.player_id "
                             "JOIN Matches M ON K.match_id=M.match_id "
                             "JOIN TournamentTeamPlayers TTP ON TTP.player_id=P.player_id "
                             "JOIN TournamentTeams TT ON TT.tournament_team_id=TTP.team_id "
                                 "AND M.tournament_id=TT.tournament_id "
                         "WHERE M.tournament_id = :tour_id  "
                             "AND team_name IN ({0}) "
                         "GROUP BY player_name ORDER BY kills DESC".format(', '.join([':'+key for key in team_list.keys()])))

            result = connection.execute(query, tour_id=tournament_id, **team_list)

            return DbUtils.fetch_without_nulls(result)

