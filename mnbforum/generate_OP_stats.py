"""
This script will generate BBCODE for overall statistics for a tournament.
It can be run either on its own - in which case it will simply print the code
for all available stats on the screen - or with command line arguments, if only
a specific subset of the stats is desired.

Please edit the following variables in __init.__py accordingly:

TOURNAMENT_NAME = name of the current tournament

Available command line arguments:
-p: generates only Player Stats
-h: generates only Hall of Fame stats
-e: generates only Eccentrics' Corner stats
"""

from __init__ import TOURNAMENT_NAME, DIV_A, DIV_B, DIV_C
from BBCodeGen import TableWithPlayersGenerator
from BBCodeGen import StatisticTableGenerator
from BBCodeGen import ThemeTableGenerator
from BBCodeGen import PlayerStatsDecorator
from PROVIDERS.PlayerStatsProvider import PlayerStatsProvider
from PROVIDERS.TournamentProvider import TournamentProvider
import sys


class OpeningPostUpdater():
    def __init__(self, tournament_name):
        tournament_provider = TournamentProvider()
        tournament = tournament_provider.provide_tournament(tournament_name)
        self.tournament_id = tournament.tournament_id
        self.stats_provider = PlayerStatsProvider()

    def generate_eccentrics_corner(self):
        stats = self.stats_provider.provide_eccentrics_corner_stats(self.tournament_id, DIV_C)

        forum_tables = list()
        for stat in stats:
            content = TableWithPlayersGenerator.generate(stat.statistic_type, stat.stats)
            forum_tables.append(StatisticTableGenerator.generate(stat.title, stat.subtitle, content))

        return ThemeTableGenerator.generate(number_of_columns=3, cells=forum_tables)

    def generate_hall_of_fame(self):
        stats = self.stats_provider.provide_hall_of_fame_stats(self.tournament_id, DIV_C)

        forum_tables = list()
        for stat in stats:
            content = TableWithPlayersGenerator.generate(stat.statistic_type, stat.stats)
            forum_tables.append(StatisticTableGenerator.generate(stat.title, stat.subtitle, content))

        return ThemeTableGenerator.generate(number_of_columns=3, cells=forum_tables)

    def generate_player_stats(self):
        player_stats = self.stats_provider.provide_player_stats(self.tournament_id, DIV_C)

        return PlayerStatsDecorator.generate_player_table(player_stats)


if __name__ == "__main__":
    updater = OpeningPostUpdater(TOURNAMENT_NAME)

    if len(sys.argv) > 1:
        if sys.argv[1] == "-p":
            print(updater.generate_player_stats())
            sys.exit(0)

        elif sys.argv[1] == "-h":
            print(updater.generate_hall_of_fame())
            sys.exit(0)

        elif sys.argv[1] == "-e":
            print(updater.generate_eccentrics_corner())
            sys.exit(0)

        else:
            print("Invalid option. Use:")
            print("-p for Player Stats")
            print("-h for Hall of Fame stats")
            print("-e for Eccentrics' Corner stats")
            sys.exit(0)

    print("[hr][/hr][img]i.ibb.co/6Y5H2XW/5yfbR36.png[/img] [b]PLAYER STATS[/b][hr][/hr]\n\n")
    print(updater.generate_player_stats())
    print("\n\n[hr][/hr][img]i.ibb.co/DwnZv95/R9G5bFY.png[/img] [b]HALL OF FAME[/b][hr][/hr]\n\n")
    print(updater.generate_hall_of_fame())
    print("\n\n[hr][/hr][img]i.ibb.co/4dfzLVD/uVmHfor.png[/img] [b]ECCENTRICS' CORNER[/b][hr][/hr]\n\n")
    print(updater.generate_eccentrics_corner())
