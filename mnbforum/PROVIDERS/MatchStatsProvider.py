from DATABASE_ACCESSORS.MatchStatsRetriever import MatchStatsRetriever
from .MatchListProvider import MatchListProvider
from .OptionalStatsHandler import OptionalStatsHandler
from STRUCTURES.MatchStats import MatchStats
from STRUCTURES.Statistic import Statistic
from STRUCTURES.StarOfTheWeek import StarOfTheWeek
from STRUCTURES.CongratulationLine import CongratulationLine

LIMIT = 10


class MatchStatsProvider():
    def __init__(self):
        self.db_accessor = MatchStatsRetriever()
        self.match_provider = MatchListProvider()

    def provide_match_stats_from_round(self, week_number, tournament_id):
        match_list = self.match_provider.provide_matches(week_number, tournament_id)

        stats_list = []
        for match in match_list:
            stats = self.__provide_single_match_stats(match)
            stats_list.append(stats)

        return stats_list

    def __provide_single_match_stats(self, match):
        team_a_kills = self.__provide_teams_match_stats(match.tournament_id, match.match_id, match.home_team)
        team_b_kills = self.__provide_teams_match_stats(match.tournament_id, match.match_id, match.away_team)

        return MatchStats(match, team_a_kills, team_b_kills)

    def __provide_teams_match_stats(self, tournament_id, match_id, team):
        kills = self.db_accessor.find_kills_for_team(tournament_id, match_id, team.name)
        assists = self.db_accessor.find_assists_for_team(tournament_id, match_id, team.name)
        classes = self.__provide_classes(self.db_accessor.find_classes_for_team(tournament_id, match_id, team.name))
        damage = self.db_accessor.find_damage_for_team(tournament_id, match_id, team.name)
        sets_played = self.db_accessor.find_sets_played_for_team(tournament_id, match_id, team.name)

        if OptionalStatsHandler.check_for_optional_stats([assists, classes, damage, sets_played]):
            kills = OptionalStatsHandler.merge_optional_stats(kills, assists, len(kills))
            kills = OptionalStatsHandler.merge_optional_stats(kills, classes, len(kills))
            kills = OptionalStatsHandler.merge_optional_stats(kills, damage, len(kills))
            kills = OptionalStatsHandler.merge_optional_stats(kills, sets_played, len(kills))

        return self.__add_team_tags(team.tag, kills)

    def __provide_classes(self, db_player_classes):
        player_classes = {}

        for row in db_player_classes:
            if row[0] not in player_classes:
                player_classes[row[0]] = [row[1]]
            else:
                player_classes[row[0]].append(row[1])

        for player in player_classes:
            for i, wb_class in enumerate(player_classes[player]):
                if wb_class in ["Infantry", "Sergeant", "Footman", "Huscarl", "Spearman"]:
                    player_classes[player][i] = 'I'
                elif wb_class in ["Archer", "Crossbowman"]:
                    player_classes[player][i] = 'A'
                else:
                    player_classes[player][i] = 'C'

        player_classes = {player: set(player_classes[player]) for player in player_classes}

        all_classes = {'I', 'A', 'C'}
        for player in player_classes:
            if all_classes & player_classes[player] == {'I'}:
                player_classes[player] = '[img]i.ibb.co/fdx7jDV/wEhjYIZ.png[/img]'
            elif all_classes & player_classes[player] == {'A'}:
                player_classes[player] = '[img]i.ibb.co/dpN97Hj/kznkpKw.png[/img]'
            elif all_classes & player_classes[player] == {'C'}:
                player_classes[player] = '[img]i.ibb.co/R9LFb1q/JcJgmFd.png[/img]'
            elif all_classes & player_classes[player] == {'I', 'A'}:
                player_classes[player] = '[img]i.ibb.co/WvqkV5Z/6QusiyI.png[/img]'
            elif all_classes & player_classes[player] == {'I', 'C'}:
                player_classes[player] = '[img]i.ibb.co/zbKLYpw/nEZTs90.png[/img]'
            elif all_classes & player_classes[player] == {'A', 'C'}:
                player_classes[player] = '[img]i.ibb.co/3s37LrD/lzKftPB.png[/img]'
            else:
                player_classes[player] = '[img]i.ibb.co/g4nLXbq/bF596jC.png[/img]'

        player_classes = [(player, player_classes[player]) for player in player_classes]
        return player_classes

    def __add_team_tags(self, tag, stats):
        tagged_stats = []

        for row in stats:
            tagged_row = []

            for count, field in enumerate(row):
                if count == 0:
                    if tag.startswith('_'):
                        field = str(field) + tag
                    else:
                        field = tag + str(field)

                tagged_row.append(field)

            tagged_stats.append(tuple(tagged_row))

        return tagged_stats

    def provide_congrats_section_data(self, tournament_id, week_number, team_list):
        congrats_lines = list()
        congrats_lines.append(
            CongratulationLine(
                names=self.__provide_melee_kills_congrats(tournament_id, week_number, team_list),
                category='Butchers'))
        congrats_lines.append(
            CongratulationLine(
                names=self.__provide_couched_lances_congrats(tournament_id, week_number, team_list),
                category='Impalers'))
        congrats_lines.append(
            CongratulationLine(
                names=self.__provide_headshots_congrats(tournament_id, week_number, team_list),
                category='Last Elves on Earth'))
        congrats_lines.append(
            CongratulationLine(
                names=self.__provide_club_20_congrats(tournament_id, week_number),
                category='Club 20'))

        star_data = self.db_accessor.find_star_of_the_week(tournament_id, week_number)
        return StarOfTheWeek(name=star_data[0], kills=star_data[1]), congrats_lines

    def __provide_headshots_congrats(self, tournament_id, week, team_list, limit=LIMIT):
        headshots = set([tup[0] for tup in self.db_accessor.find_headshots_congrats(tournament_id, week, team_list, limit)])
        past_headshots = set([tup[0] for tup in self.db_accessor.find_headshots_congrats(tournament_id, int(week)-1, team_list, limit)])

        return list(headshots.difference(past_headshots))

    def __provide_couched_lances_congrats(self, tournament_id, week, team_list, limit=LIMIT):
        couched_lances = set([tup[0] for tup in self.db_accessor.find_couched_lances_congrats(tournament_id, week, team_list, limit)])
        past_couched_lances = set([tup[0] for tup in self.db_accessor.find_couched_lances_congrats(tournament_id, int(week)-1, team_list, limit)])

        return list(couched_lances.difference(past_couched_lances))

    def __provide_melee_kills_congrats(self, tournament_id, week, team_list, limit=LIMIT):
        melee_kills = set([tup[0] for tup in self.db_accessor.find_melee_kills_congrats(tournament_id, week, team_list, limit)])
        past_melee_kills = set([tup[0] for tup in self.db_accessor.find_melee_kills_congrats(tournament_id, int(week)-1, team_list, limit)])

        return list(melee_kills.difference(past_melee_kills))

    def __provide_club_20_congrats(self, tournament_id, week):
        stats = self.__find_club_20(tournament_id, week)
        past_stats = self.__find_club_20(tournament_id, int(week) - 1)

        return list(set([tup[0] for tup in stats]).difference(set([tup[0] for tup in past_stats])))

    def __find_club_20(self, tournament_id, week):
        stats = []
        for week in range(1, int(week) + 1):
            stats.extend(self.db_accessor.find_club_20(tournament_id, week))
        stats.sort(key=lambda tup: tup[1], reverse=True)
        return stats

    def provide_elite_stats(self, tournament_id, week_number):
        stats = list()

        stats.append(self.__provide_melee_kills_stat(tournament_id, week_number))
        stats.append(self.__provide_couched_lances_stat(tournament_id, week_number))
        stats.append(self.__provide_headshot_stat(tournament_id, week_number))
        stats.append(self.__provide_club_20_stat(tournament_id, week_number))

        return stats

    def __provide_headshot_stat(self, tournament_id, week, limit=LIMIT):
        return Statistic(
            title="Headshots",
            subtitle="This week's top 10 headshots.",
            statistic_type=["K"],
            stats=self.db_accessor.find_headshots(tournament_id, week, limit))

    def __provide_couched_lances_stat(self, tournament_id, week, limit=LIMIT):
        return Statistic(
            title="Couched Lances",
            subtitle="This week's top 10 couched lance kills.",
            statistic_type=["K"],
            stats=self.db_accessor.find_couched_lances(tournament_id, week, limit))

    def __provide_melee_kills_stat(self, tournament_id, week, limit=LIMIT):
        return Statistic(
            title="Melee Kills",
            subtitle="This week's top 10 1h weapon kills.",
            statistic_type=["K"],
            stats=self.db_accessor.find_melee_kills(tournament_id, week, limit))

    def __provide_club_20_stat(self, tournament_id, week):
        return Statistic(
            title="Club 20",
            subtitle="Players with 20+ kills per match.",
            statistic_type=["K"],
            stats=self.db_accessor.find_club_20(tournament_id, week))
