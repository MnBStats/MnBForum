from Exceptions import OptionalStatsError


class OptionalStatsHandler:
    def __init__(self):
        pass

    @staticmethod
    def check_for_optional_stats(optional_stats_list=[]):
        checksum = len(optional_stats_list)
        stat_count = 0

        for stat in optional_stats_list:
            if len(stat) != 0:
                stat_count = stat_count + 1

        if stat_count == checksum:
            return True
        elif stat_count == 0:
            return False
        else:
            raise OptionalStatsError("Optional stats only partially present. " + str(checksum-stat_count) + " of the optional stat categories are empty.")

    @staticmethod
    def merge_optional_stats(core_stats, optional_stats, pos):
        if len(core_stats) != len(optional_stats):
            raise OptionalStatsError("OptionalStatsHandler cannot perform the merge operation becuase the lengths of core and optional stats differ.")

        core_stats = {tup[0]: list(tup[1:]) for tup in core_stats}
        optional_stats = {tup[0]: list(tup[1:]) for tup in optional_stats}

        for key in core_stats:
            for item in optional_stats[key]:
                core_stats[key].insert(pos, item)

        merged_stats = []
        for i, key in enumerate(core_stats):
            merged_stats.append([key])
            merged_stats[i].extend(core_stats[key])

        merged_stats.sort(key=lambda stat: stat[1], reverse=True)
        return [tuple(stat) for stat in merged_stats]
