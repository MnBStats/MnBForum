from Exceptions import LogicError
from DATABASE_ACCESSORS.TournamentInfoChecker import TournamentInfoChecker
from STRUCTURES.Tournament import Tournament


class TournamentProvider():
    def __init__(self):
        self.db_accessor = TournamentInfoChecker()

    def provide_tournament(self, tournament_name):
        tournament_id = self.db_accessor.find_tournament_id(tournament_name)

        if tournament_id is None:
            raise LogicError("Tournament " + tournament_name + " not found.")

        return Tournament(name=tournament_name, tournament_id=tournament_id)

    def provide_tournaments(self):
        tournaments = []

        for tournament in self.db_accessor.find_tournaments():
            tournaments.append(Tournament(str(tournament[0]), str(tournament[1])))

        return tournaments
