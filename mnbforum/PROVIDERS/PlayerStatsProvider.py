from DATABASE_ACCESSORS.PlayerStatsRetriever import PlayerStatsRetriever
from .OptionalStatsHandler import OptionalStatsHandler
from STRUCTURES.Statistic import Statistic

LIMIT = 10


class PlayerStatsProvider():
    def __init__(self):
        self.db_accessor = PlayerStatsRetriever()

    def provide_player_stats(self, tournament_id, team_list):
        player_stats = self.db_accessor.find_player_stats(tournament_id, team_list)
        assists = self.db_accessor.find_assists(tournament_id, team_list, limit=len(player_stats))

        if OptionalStatsHandler.check_for_optional_stats([assists]):
            player_stats = OptionalStatsHandler.merge_optional_stats(player_stats, assists, 3)

        return player_stats[:100]

    def provide_hall_of_fame_stats(self, tournament_id, team_list):
        stats = list()
        stats.append(self.__provide_melee_kills_stat(tournament_id, team_list))
        stats.append(self.__provide_couched_lances_stat(tournament_id, team_list))
        stats.append(self.__provide_headshot_stat(tournament_id, team_list))
        stats.append(self.__provide_kill_streaks_stat(tournament_id, team_list))
        stats.append(self.__provide_club_20_stat(tournament_id, team_list))

        optional_stats = [self.__provide_assist_stat(tournament_id, team_list)]
        if OptionalStatsHandler.check_for_optional_stats([opstat.stats for opstat in optional_stats]):
            stats.extend(optional_stats)

        return stats

    def __provide_headshot_stat(self, tournament_id, team_list, limit=LIMIT):
        return Statistic(
            title="Last Elves on Earth",
            subtitle="Top 10 headshots.",
            statistic_type=["K"],
            stats=self.db_accessor.find_headshots(tournament_id, team_list, limit))

    def __provide_couched_lances_stat(self, tournament_id, team_list, limit=LIMIT):
        return Statistic(
            title="Impalers",
            subtitle="Top 10 couched lance kills.",
            statistic_type=["K"],
            stats=self.db_accessor.find_couched_lances(tournament_id, team_list, limit))

    def __provide_melee_kills_stat(self, tournament_id, team_list, limit=LIMIT):
        return Statistic(
            title="Butchers",
            subtitle="Top 10 1h weapon kills.",
            statistic_type=["K"],
            stats=self.db_accessor.find_melee_kills(tournament_id, team_list, limit))

    def __provide_kill_streaks_stat(self, tournament_id, team_list):
        data = self.db_accessor.find_kill_streaks(tournament_id, team_list)

        kill_streaks = []
        for row in data:
            if any(row in x for x in kill_streaks):
                i = [tup[0] for tup in kill_streaks].index(row)
                kill_streaks[i] = (row, kill_streaks[i][1] + 1)

            else:
                kill_streaks.append((row, 1))

        kill_streaks.sort(key=lambda tup: tup[1], reverse=True)
        kill_streaks = [player for count, player in enumerate(kill_streaks) if player[0][0] not in [tup[0][0] for tup in kill_streaks][:count]]

        return Statistic(
            title="Serial Killers",
            subtitle="Top 10 killstreaks in a single round.",
            statistic_type=["K"],
            stats=[(tup[0][0], tup[1]) for tup in kill_streaks][:10])

    def __provide_club_20_stat(self, tournament_id, team_list):
        data = self.db_accessor.find_club_20(tournament_id, team_list)

        return Statistic(
            title="Club 20",
            subtitle="Players with 20+ kills per match.",
            statistic_type=["K"],
            stats=[player for count, player in enumerate(data) if player[0] not in [tup[0] for tup in data[0:count]]])

    def __provide_assist_stat(self, tournament_id, team_list, limit=LIMIT):
        return Statistic(
            title="Team Players",
            subtitle="Top 10 assists.",
            statistic_type=["A"],
            stats=self.db_accessor.find_assists(tournament_id, team_list, limit))

    def provide_eccentrics_corner_stats(self, tournament_id, team_list):
        eccentrics_stats = list()
        eccentrics_stats.append(self.__provide_hammer_stat(tournament_id, team_list))
        eccentrics_stats.append(self.__provide_teamkills_stat(tournament_id, team_list))
        eccentrics_stats.append(self.__provide_teamkilled_stat(tournament_id, team_list))
        eccentrics_stats.append(self.__provide_2h_stat(tournament_id, team_list))
        eccentrics_stats.append(self.__provide_first_kill_stat(tournament_id, team_list))
        eccentrics_stats.append(self.__provide_first_deaths_stat(tournament_id, team_list))
        eccentrics_stats.append(self.__provide_morningstar_stat(tournament_id, team_list))
        eccentrics_stats.append(self.__provide_unarmed_stat(tournament_id, team_list))
        eccentrics_stats.append(self.__provide_kd_ratio_stat(tournament_id, team_list))

        optional_stats = [self.__provide_spear_stat(tournament_id, team_list), self.__provide_throwing_weapons_stat(tournament_id, team_list), \
                     self.__provide_damage_stat(tournament_id, team_list)]
        if OptionalStatsHandler.check_for_optional_stats([opstat.stats for opstat in optional_stats]):
            eccentrics_stats.extend(optional_stats)

        return eccentrics_stats

    def __provide_hammer_stat(self, tournament_id, team_list, limit=LIMIT):
        return Statistic(
            title="Stop! Hammer Time",
            subtitle="Top 10 hammer kills.",
            statistic_type=["K"],
            stats=self.db_accessor.find_hammer_kills(tournament_id, team_list, limit))

    def __provide_2h_stat(self, tournament_id, team_list, limit=LIMIT):
        return Statistic(
            title="D00l Kids",
            subtitle="Top 10 2h blade weapon kills.",
            statistic_type=["K"],
            stats=self.db_accessor.find_2h_kills(tournament_id, team_list, limit))

    def __provide_teamkills_stat(self, tournament_id, team_list, limit=LIMIT):
        return Statistic(
            title="Berserkers",
            subtitle="Top 10 teamkillers.",
            statistic_type=["TK"],
            stats=self.db_accessor.find_teamkills(tournament_id, team_list, limit))

    def __provide_teamkilled_stat(self, tournament_id, team_list, limit=LIMIT):
        return Statistic(
            title="The Hexed",
            subtitle="Top 10 most teamkilled players.",
            statistic_type=["D"],
            stats=self.db_accessor.find_teamkilled(tournament_id, team_list, limit))

    def __provide_first_kill_stat(self, tournament_id, team_list, limit=LIMIT):
        return Statistic(
            title="Initiators",
            subtitle="Top 10 players who get first kill most often.",
            statistic_type=["K"],
            stats=self.db_accessor.find_first_kills(tournament_id, team_list, limit))

    def __provide_first_deaths_stat(self, tournament_id, team_list, limit=LIMIT):
        return Statistic(
            title="Shield Stealers",
            subtitle="Top 10 players who die first the most.",
            statistic_type=["D"],
            stats=self.db_accessor.find_first_deaths(tournament_id, team_list, limit))

    def __provide_morningstar_stat(self, tournament_id, team_list, limit=LIMIT):
        return Statistic(
            title="Clubbers",
            subtitle="Top 10 morning star kills.",
            statistic_type=["K"],
            stats=self.db_accessor.find_morningstar_kills(tournament_id, team_list, limit))

    def __provide_spear_stat(self, tournament_id, team_list, limit=LIMIT):
        return Statistic(
            title="Hoplites",
            subtitle="Top 10 spear kills.",
            statistic_type=["K"],
            stats=self.db_accessor.find_spear_kills(tournament_id, team_list, limit))

    def __provide_throwing_weapons_stat(self, tournament_id, team_list, limit=LIMIT):
        return Statistic(
            title="Skirmishers",
            subtitle="Top 10 throwing weapon kills.",
            statistic_type=["K"],
            stats=self.db_accessor.find_throwing_weapon_kills(tournament_id, team_list, limit))

    def __provide_damage_stat(self, tournament_id, team_list, limit=LIMIT):
        return Statistic(
            title="Hulk SMASH!",
            subtitle="Top 10 players with highest damage.",
            statistic_type=["Dmg"],
            stats=self.db_accessor.find_damage(tournament_id, team_list, limit))

    def __provide_unarmed_stat(self, tournament_id, team_list, limit=LIMIT):
        return Statistic(
            title="Kung-Fu Masters",
            subtitle="Top 10 unarmed kills.",
            statistic_type=["K"],
            stats=self.db_accessor.find_unarmed_kills(tournament_id, team_list, limit))

    def __provide_kd_ratio_stat(self, tournament_id, team_list):
        data = self.db_accessor.find_kd_ratio(tournament_id, team_list)

        kd_list = []
        for row in data:
            kd_ratio = 0
            if row[2] != 0:
                kd_ratio = round(row[1]/row[2], 1)

                if kd_ratio*10 % 10 == 0:
                    kd_ratio = int(kd_ratio)

            else:
                kd_ratio = -1

            kd_list.append((row[0], kd_ratio))

        kd_list.sort(key=lambda tup: float(tup[1]), reverse=True)
        kd_list = ['∞' if kd==-1 else kd for kd in kd_list]

        return Statistic(
            title="Most Rational Players",
            subtitle="Top 10 players with highest k/d ratio.",
            statistic_type=["KD"],
            stats=kd_list[:10])
