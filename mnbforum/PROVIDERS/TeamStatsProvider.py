#-*- coding: utf-8 -*-

from DATABASE_ACCESSORS.TeamStatsRetriever import TeamStatsRetriever


class TeamStatsProvider:
    def __init__(self, team):
        self.team = team
        self.db_accessor = TeamStatsRetriever(team.team_id)

    def provide_total_kills(self):
        return self.db_accessor.find_total_kills()

    def provide_kd(self):
        total_kills = self.db_accessor.find_total_kills()
        total_deaths = self.db_accessor.find_total_deaths()

        kd_ratio = 0
        if total_deaths != 0:
            kd_ratio = round(float(total_kills)/float(total_deaths), 1)

            if kd_ratio*10 % 10 == 0:
                kd_ratio = int(kd_ratio)

        else:
            kd_ratio = '∞'

        return kd_ratio

    def provide_kill_types(self):
        data = self.db_accessor.find_kill_types()

        return [(tup[0][9:-1], tup[1]) for tup in data]

    def provide_number_of_matches(self):
        return  self.db_accessor.find_number_of_matches()

    def provide_strongest_map(self):
        spawns = self.db_accessor.find_spawn_info()
        map_data = {}

        for spawn in spawns:
            total_kills = self.db_accessor.find_kills_for_spawn(spawn[0], spawn[1])
            total_deaths = self.db_accessor.find_deaths_for_spawn(spawn[0], spawn[1])
            oponent_id = spawn[3] if self.team.team_id != spawn[3] else spawn[5]

            if total_deaths == 0:
                total_deaths = 1

            weight = self.__calculate_weight(oponent_id)
            map_score = float(total_kills) / float(total_deaths) * weight

            if spawn[2] not in map_data:
                map_data[spawn[2]] = [map_score]

            else:
                map_data[spawn[2]].append(map_score)

        for map in map_data:
            map_data[map] = sum(map_data[map]) / len(map_data[map])

        map_data = map_data.items()
        map_data.sort(key=lambda tup: tup[1], reverse=True)

        return map_data[0][0]

    def provide_strongest_faction(self):
        spawns = self.db_accessor.find_spawn_info()
        faction_data = {}

        for spawn in spawns:
            total_kills = self.db_accessor.find_kills_for_spawn(spawn[0], spawn[1])
            total_deaths = self.db_accessor.find_deaths_for_spawn(spawn[0], spawn[1])
            oponent_id = spawn[3] if self.team.team_id != spawn[3] else spawn[5]
            faction = spawn[4] if self.team.team_id == spawn[3] else spawn[6]

            if total_deaths == 0:
                total_deaths = 1

            weight = self.__calculate_weight(oponent_id)
            faction_score = float(total_kills) / float(total_deaths) * weight

            if faction not in faction_data:
                faction_data[faction] = [faction_score]

            else:
                faction_data[faction].append(faction_score)

        for fac in faction_data:
            faction_data[fac] = sum(faction_data[fac]) / len(faction_data[fac])

        faction_data = faction_data.items()
        faction_data.sort(key=lambda tup: tup[1], reverse=True)

        return faction_data[0][0]

    def __calculate_weight(self, oponent_id):
        team_level = self.__determine_team_level(self.team.team_id)
        oponent_level = self.__determine_team_level(oponent_id)

        return 1 - (team_level - oponent_level) * 0.125

    def __determine_team_level(self, team_id):
        match_scores = TeamStatsRetriever(team_id).find_match_scores()
        wins = 0

        for score in match_scores:
            team_score = score[1] if team_id == score[0] else score[3]
            opponent_score = score[1] if team_id != score[0] else score[3]

            if team_score > opponent_score:
                wins = wins + 1

        return wins
