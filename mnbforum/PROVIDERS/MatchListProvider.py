from DATABASE_ACCESSORS.TournamentInfoChecker import TournamentInfoChecker
from .TeamProvider import TeamProvider
from .TournamentProvider import TournamentProvider
from STRUCTURES.Match import Match


class MatchListProvider():
    def __init__(self):
        self.db_accessor = TournamentInfoChecker()
        self.team_provider = TeamProvider()
        self.tour_provider = TournamentProvider()

    def provide_matches_from_round(self, round_number, tournament_name):
        tournament = self.tour_provider.provide_tournament(tournament_name)

        return self.provide_matches(round_number, tournament.tournament_id)

    def provide_matches(self, round_number, tournament_id):
        data = self.db_accessor.find_matches_from_round(
            round_number=round_number,
            tournament_id=tournament_id
        )

        matches = []
        for match_data in data:
            team_a = self.team_provider.provide_team_by_id(match_data[2])
            team_b = self.team_provider.provide_team_by_id(match_data[3])

            match = Match(
                match_id=match_data[0],
                tournament_id=match_data[1],
                team_a=team_a,
                team_b=team_b,
                home_score=match_data[4],
                away_score=match_data[5]
            )

            if match.home_team_score != 0 or match.away_team_score != 0:
                matches.append(match)

        return matches
