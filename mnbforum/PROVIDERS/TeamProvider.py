from STRUCTURES.Team import Team
from DATABASE_ACCESSORS.TournamentInfoChecker import TournamentInfoChecker


# TODO: Team provider does not need to be a class
class TeamProvider():
    def __init__(self, db_accessor=TournamentInfoChecker()):
        self.db_accessor = db_accessor

    def provide_team_by_id(self, team_id):
        team_data = self.db_accessor.find_team(team_id)

        return Team(
            name=team_data[2],
            tag=team_data[3],
            team_id=team_data[0],
            tournament_id=team_data[1]
        )

    def provide_team(self, team_name, tournament_id):
        team_id = self.db_accessor.find_team_id(team_name, tournament_id)

        if team_id is not None:
            return self.provide_team_by_id(team_id)

        return None

    def provide_player_teams(self, player_id):
        teams = list()
        teams_ids = self.db_accessor.find_player_teams(player_id)
        for team_id in teams_ids:
            teams.append(self.provide_team_by_id(team_id))

        return teams

    def provide_tournament_teams(self, tournament_id):
        teams_ids = self.db_accessor.find_tournament_teams(tournament_id)

        tournament_teams = []
        for team_id in teams_ids:
            tournament_teams.append(self.provide_team_by_id(team_id))

        return tournament_teams
