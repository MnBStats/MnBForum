#-*- coding: utf-8 -*-

from DATABASE_ACCESSORS.FinalsStatsRetriever import FinalsStatsRetriever
from .TeamProvider import TeamProvider
from .OptionalStatsHandler import OptionalStatsHandler
from STRUCTURES.Score import Score


class FinalsStatsProvider():
    def __init__(self, finals_match_id):
        self.match_id = finals_match_id
        self.db_accessor = FinalsStatsRetriever(finals_match_id)

    def provide_number_of_spawns(self):
        return self.db_accessor.find_number_of_spawns()

    def provide_spawn_info(self, spawn_number):
        team_a, team_b = self.__find_team_order_for_spawn(spawn_number)
        spawn_score = self.__provide_spawn_score(spawn_number)
        team_provider = TeamProvider()

        return team_provider.provide_team_by_id(team_a), team_provider.provide_team_by_id(team_b), spawn_score

    def __find_team_order_for_spawn(self, spawn_number):
        attacking_team = self.db_accessor.find_attacking_team(spawn_number)
        defending_team = self.db_accessor.find_defending_team(spawn_number)

        return attacking_team, defending_team

    def __provide_spawn_score(self, spawn_number):
        score = self.db_accessor.find_spawn_score(spawn_number)

        return Score(score[0][0], score[0][1])

    def provide_team_stats_for_spawn(self, team, spawn_number):
        kills = self.db_accessor.find_team_kills_for_spawn(team.name, spawn_number)
        assists = self.db_accessor.find_team_assists_for_spawn(team.name, spawn_number)
        classes = self.__provide_classes(self.db_accessor.find_team_classes_for_spawn(team.name, spawn_number))

        if OptionalStatsHandler.check_for_optional_stats([assists, classes]):
            kills = OptionalStatsHandler.merge_optional_stats(kills, assists, len(kills[0]))
            kills = OptionalStatsHandler.merge_optional_stats(kills, classes, len(kills[0]))

        return kills

    def __provide_classes(self, db_player_classes):
        player_classes = {}

        for row in db_player_classes:
            if row[0] not in player_classes:
                player_classes[row[0]] = [row[1]]
            else:
                player_classes[row[0]].append(row[1])

        for player in player_classes:
            for i, wb_class in enumerate(player_classes[player]):
                if wb_class in ["Infantry", "Sergeant", "Footman", "Huscarl", "Spearman"]:
                    player_classes[player][i] = 'I'
                elif wb_class in ["Archer", "Crossbowman"]:
                    player_classes[player][i] = 'A'
                else:
                    player_classes[player][i] = 'C'

        player_classes = {player: set(player_classes[player]) for player in player_classes}

        all_classes = {'I', 'A', 'C'}
        for player in player_classes:
            if all_classes & player_classes[player] == {'I'}:
                player_classes[player] = '[img]i.ibb.co/1n3r6fQ/wEhjYIZ.png[/img]'
            elif all_classes & player_classes[player] == {'A'}:
                player_classes[player] = '[img]i.ibb.co/dpN97Hj/kznkpKw.png[/img]'
            elif all_classes & player_classes[player] == {'C'}:
                player_classes[player] = '[img]i.ibb.co/R9LFb1q/JcJgmFd.png[/img]'
            elif all_classes & player_classes[player] == {'I', 'A'}:
                player_classes[player] = '[img]i.ibb.co/WvqkV5Z/6QusiyI.png[/img]'
            elif all_classes & player_classes[player] == {'I', 'C'}:
                player_classes[player] = '[img]i.ibb.co/zbKLYpw/nEZTs90.png[/img]'
            elif all_classes & player_classes[player] == {'A', 'C'}:
                player_classes[player] = '[img]i.ibb.co/3s37LrD/lzKftPB.png[/img]'
            else:
                player_classes[player] = '[img]i.ibb.co/g4nLXbq/bF596jC.png[/img]'

        player_classes = [(player, player_classes[player]) for player in player_classes]
        return player_classes

    def provide_team_kd_for_spawn(self, team, spawn_number):
        total_kills = self.db_accessor.find_team_total_kills_for_spawn(team.team_id, spawn_number)
        total_deaths = self.db_accessor.find_team_total_deaths_for_spawn(team.team_id, spawn_number)

        kd_ratio = 0
        if total_deaths != 0:
            kd_ratio = round(float(total_kills)/float(total_deaths), 1)

            if kd_ratio*10 % 10 == 0:
                kd_ratio = int(kd_ratio)

        else:
            kd_ratio = '∞'

        return kd_ratio

    def provide_team_elite_stats_for_spawn(self, team, spawn_number):
        stats = list()
        stats.append(self.__provide_team_melee_kills_for_spawn(team, spawn_number))
        stats.append(self.__provide_team_couched_lances_for_spawn(team, spawn_number))
        stats.append(self.__provide_team_headshots_for_spawn(team, spawn_number))
        stats.append(self.__provide_team_kill_streaks_for_spawn(team, spawn_number))

        return stats

    def __provide_team_melee_kills_for_spawn(self, team, spawn_number):
        return self.db_accessor.find_team_melee_kills_for_spawn(team.team_id, spawn_number)

    def __provide_team_couched_lances_for_spawn(self, team, spawn_number):
        return self.db_accessor.find_team_couched_lances_for_spawn(team.team_id, spawn_number)

    def __provide_team_headshots_for_spawn(self, team, spawn_number):
        return self.db_accessor.find_team_headshots_for_spawn(team.team_id, spawn_number)

    def __provide_team_kill_streaks_for_spawn(self, team, spawn_number):
        data = self.db_accessor.find_team_kill_streaks_for_spawn(team.team_id, spawn_number)

        kill_streaks = []
        for row in data:
            if any(row in x for x in kill_streaks):
                i = [tup[0] for tup in kill_streaks].index(row)
                kill_streaks[i] = (row, kill_streaks[i][1] + 1)

            else:
                kill_streaks.append((row, 1))

        kill_streaks.sort(key=lambda tup: tup[1], reverse=True)
        kill_streaks = [player for count, player in enumerate(kill_streaks) if player[0][0] not in [tup[0][0] for tup in kill_streaks][:count]]

        return [(tup[0][0], tup[1]) for tup in kill_streaks]

    def provide_team_kill_distribution_section_for_spawn(self, team, spawn_number):
        chart_data = self.__provide_team_kill_types_for_spawn(team, spawn_number)

        melee_kills = self.__calculate_team_percent_melee_kills_for_spawn(team, spawn_number)
        ranged_kills = self.__calculate_team_percent_ranged_kills_for_spawn(team, spawn_number)
        cav_kills = self.__calculate_team_percent_cav_kills_for_spawn(team, spawn_number)

        return chart_data, (melee_kills, ranged_kills, cav_kills)

    def __provide_team_kill_types_for_spawn(self, team, spawn_number):
        data = self.db_accessor.find_team_kill_types_for_spawn(team.team_id, spawn_number)

        return [(tup[0][9:-1], tup[1]) for tup in data]

    def __calculate_team_percent_melee_kills_for_spawn(self, team, spawn_number):
        melee_kills = self.db_accessor.find_team_total_melee_kills_for_spawn(team.team_id, spawn_number)
        total_kills = self.db_accessor.find_team_total_kills_for_spawn(team.team_id, spawn_number)

        if total_kills != 0:
            return int(round((float(melee_kills)/float(total_kills)) * 100, 1))

        else:
            return 0

    def __calculate_team_percent_ranged_kills_for_spawn(self, team, spawn_number):
        ranged_kills = self.db_accessor.find_team_total_ranged_kills_for_spawn(team.team_id, spawn_number)
        total_kills = self.db_accessor.find_team_total_kills_for_spawn(team.team_id, spawn_number)

        if total_kills != 0:
            return int(round((float(ranged_kills)/float(total_kills)) * 100, 1))

        else:
            return 0

    def __calculate_team_percent_cav_kills_for_spawn(self, team, spawn_number):
        cav_kills = self.db_accessor.find_team_total_cav_kills_for_spawn(team.team_id, spawn_number)
        total_kills = self.db_accessor.find_team_total_kills_for_spawn(team.team_id, spawn_number)

        if total_kills != 0:
            return int(round((float(cav_kills)/float(total_kills)) * 100, 1))

        else:
            return 0

    def provide_spawn_analysis_for_team(self, team, spawn_number):
        number_of_rounds = self.db_accessor.find_number_of_rounds(spawn_number)

        rounds_with_tk = self.__calculate_number_of_rounds_with_tk(team, spawn_number, number_of_rounds)
        rounds_with_adv = self.__calculate_number_of_rounds_with_early_advantage(team, spawn_number, number_of_rounds)
        rounds_with_disadv = self.__calculate_number_of_rounds_with_early_disadvantage(team, spawn_number, number_of_rounds)
        biggest_disadv = self.__calculate_biggest_disadvantage_for_team(team, spawn_number, number_of_rounds)

        return rounds_with_tk, rounds_with_adv, rounds_with_disadv, biggest_disadv

    def __calculate_number_of_rounds_with_tk(self, team, spawn_number, number_of_rounds):
        count = 0
        for round in range(0, number_of_rounds):
            tk_count = self.db_accessor.find_team_teamkills_for_round(team.team_id, spawn_number, round+1)

            if tk_count > 0:
                count = count + 1

        return count

    def __calculate_number_of_rounds_with_early_advantage(self, team, spawn_number, number_of_rounds):
        count = 0
        for round in range(0, number_of_rounds):
            first_kill = self.db_accessor.find_first_kill_for_round(spawn_number, round+1)

            if (int(first_kill[0][0]) == int(team.team_id)) and (first_kill[0][1] != 1):
                count = count + 1

        return count

    def __calculate_number_of_rounds_with_early_disadvantage(self, team, spawn_number, number_of_rounds):
        count = 0
        for round in range(0, number_of_rounds):
            first_kill = self.db_accessor.find_first_kill_for_round(spawn_number, round+1)

            if (int(first_kill[0][0]) != int(team.team_id)) and (first_kill[0][1] != 1):
                count = count + 1

        return count

    def __calculate_biggest_disadvantage_for_team(self, team, spawn_number, number_of_rounds):
        data = []
        for round in range(0, number_of_rounds):
            data.append(self.db_accessor.find_team_kills_for_round(spawn_number, round+1))

        res = []
        for round in data:
            count = 0
            gap = 0
            for kill in round:
                if(int(kill[0]) != int(team.team_id)) and (kill[1]-gap == 1):
                    count = count + 1
                    gap = gap + 1

            res.append(count)

        return sorted(res, reverse=True)[0]
