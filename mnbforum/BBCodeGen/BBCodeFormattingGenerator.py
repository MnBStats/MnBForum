def make_bold(text):
    return "[b]" + str(text) + "[/b]"

def make_italic(text):
    return "[i]" + str(text) + "[/i]"

def insert_horizontal_line():
    return "[hr][/hr]"

def center(text):
    return "[center]" + str(text) + "[/center]"

def put_inside_spoiler(text):
    return "[spoiler]" + str(text) + "[/spoiler]"

def change_size(size, text):
    return "[size=" + str(size) + "pt]" + str(text) + "[/size]"

def change_font(font, text):
    return "[font=" + font + "]" + str(text) + "[/font]"

def change_colour(colour, text):
    return "[color=" + colour + "]" + text + "[/color]"
