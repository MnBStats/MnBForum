from . import TableWithPlayersGenerator
from . import StatisticTableGenerator
from . import ThemeTableGenerator


def generate_whole_week(match_stats_list, week_number):
    match_tables = []
    for match_stats in match_stats_list:
        if match_stats.team_a_kills and match_stats.team_b_kills:
            match_tables.append(__generate_one_match(match_stats))

    title = "[hr][/hr][center][font=georgia][size=32px]WEEK {0}[/size][/font][/center][hr][/hr]\n\n".format(week_number)
    bb_code = list(title)
    bb_code.append(ThemeTableGenerator.generate(number_of_columns=2, cells=match_tables))

    return "".join(bb_code)


def __generate_one_match(match_stats):
    return StatisticTableGenerator.generate(
        title=match_stats.title(),
        subtitle="",
        content=__generate_match_table(match_stats.team_a_kills, match_stats.team_b_kills))


def __generate_match_table(team_a_stats, team_b_stats):
    header = ["K", "D", "TK"]

    if len(team_a_stats[0]) > 4:
        header.extend(["A", "Classes", "Dmg", "Pld"])

    first_cell_spacer = " " * 5
    cells = [
        TableWithPlayersGenerator.generate(header=header, table=team_a_stats, spacer=first_cell_spacer),
        TableWithPlayersGenerator.generate(header=header, table=team_b_stats, spacer=first_cell_spacer)]

    space_between_cells = " " * 10
    return ThemeTableGenerator.generate(number_of_columns=2, cells=cells, spacer=space_between_cells)


