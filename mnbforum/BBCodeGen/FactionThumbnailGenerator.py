FACTIONS = {'Kingdom of Swadia': 'i.ibb.co/2tD65NW/Swadien-Logo.png',
            'Sarranid Sultanate': 'i.ibb.co/zJ7FVhG/5PoGB3h.png',
            'Kingdom of Vaegirs': 'i.ibb.co/fpX2C12/VAEGIR-logo-c.png',
            'Kingdom of Rhodoks': 'i.ibb.co/7gfGmZD/rhodok.png',
            'Kingdom of Nords': 'i.ibb.co/9YKS4LW/nord.png'}


def generate(faction):
    return "[img width=65]" + FACTIONS[faction] + "[/img]"
