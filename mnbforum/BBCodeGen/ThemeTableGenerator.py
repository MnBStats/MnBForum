from Exceptions import InvalidInputError

SPACER = " " * 8


def generate(number_of_columns, cells, spacer=SPACER):
    if number_of_columns <= 0:
        raise InvalidInputError("ThemeTableGenerator: Number of columns must be greater than 0.")

    return "[center][table=noborder]\n{0}[/table][/center]".format(__generate_rows(number_of_columns, cells, spacer))


def __generate_rows(number_of_columns, cells, spacer):
    if len(cells) <= number_of_columns:
        return __generate_row(cells, spacer)

    return __generate_row(cells[:number_of_columns], spacer) +\
        __generate_rows(number_of_columns, cells[number_of_columns:len(cells)], spacer)


def __generate_row(cells, spacer):
    return "[tr]{0}[/tr]\n".format(__generate_cells(cells, spacer))


def __generate_cells(cells, spacer):
    output = list()
    for cell in cells:
        output.append(__generate_cell(cell))

        if cell != cells[-1]:
            output.append("[td]{0}[/td]".format(spacer))

    return "".join(output)


def __generate_cell(cell):
    return "[td]{0}[/td]".format(cell)
