from . import HeaderGenerator

SPACER = " " * 25
SMALL_SPACER = " " * 2


def generate(header, table, spacer=SPACER, small_spacer=SMALL_SPACER):
    bbcode_table = list()

    bbcode_table.append("[table=noborder]\n")

    # TODO: This class should create table with players only without header. Than you can have
    # composer class that will use those tables they like.
    if header:
        bbcode_table.append(HeaderGenerator.generate(header, spacer, small_spacer))

    for row in table:
        bbcode_table.append(__generate_row(row, spacer, small_spacer))

    bbcode_table.append("[/table]")
    return "".join(bbcode_table)


def __generate_row(row, spacer, small_spacer):
    bbcode_row = list()
    bbcode_row.append("[tr][td]{name}{spacer}[/td]".format(name=row[0], spacer=spacer))
    for field in row[1:]:
        bbcode_row.append(__generate_field(field, small_spacer))

    bbcode_row.append("[/tr]\n")
    return "".join(bbcode_row)


def __generate_field(field, small_spacer):
    return "[td]{spacer}{value}{spacer}[/td]".format(spacer=small_spacer, value=str(field))
