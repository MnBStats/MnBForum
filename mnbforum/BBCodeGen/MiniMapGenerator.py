__author__ = 'yami_pirate'

MAPS = {'Fort of Honour': 'http://i993.photobucket.com/albums/af59/Erminas/Map-pictures-Fort-of-Honour_zpsohqmccrq.png',
        'Frosthaven': 'http://i993.photobucket.com/albums/af59/Erminas/Map-pictures-Frosthaven_zpswm0nlrve.png',
        'Castellum': 'http://i993.photobucket.com/albums/af59/Erminas/Map-pictures-Castellum_zpsw1xumjw0.png',
        'Verloren': 'http://i993.photobucket.com/albums/af59/Erminas/Map-pictures-Verloren_zps856nbmuq.png',
        'Naval Outpost': 'http://i993.photobucket.com/albums/af59/Erminas/Map-pictures-Naval-Outpost_zpsdnhnkral.png',
        'River Village': 'http://i993.photobucket.com/albums/af59/Erminas/Map-pictures-River-Village_zpsa85mx4nu.png',
        'Reveran Village': 'http://i993.photobucket.com/albums/af59/Erminas/Map-pictures-Reveran-Village_zpslpczrf64.png',
        'Desert Oasis': 'https://dl.dropboxusercontent.com/s/7fmahl5mgxdatot/SHITMAPFIXED.png',
        'Mountain Fortress': 'http://i993.photobucket.com/albums/af59/Erminas/mountain-fort-map_zpswgz5tmdb.png',
        'Naval Outpost': 'http://i993.photobucket.com/albums/af59/Erminas/Map-pictures-Naval-Outpost_zpsdnhnkral.png',
        'San\'di\'boush': 'http://i993.photobucket.com/albums/af59/Erminas/Map-pictures-Sandi_zpsduiqznyp.png',
        'Ruins': 'http://i993.photobucket.com/albums/af59/Erminas/Map-pictures-Ruins_zpsivxq0hn2.png',
        'Nord Town': 'https://dl.dropboxusercontent.com/s/oa3gh331yeve75w/NREDTOWNFIXEDFIXED.png',
        'Beairteas': 'i.ibb.co/Wn0cK94/Bairteas.png',
        'Waterfall': 'i.ibb.co/HgLdw4X/Waterfall.png',
        'Winterburg': 'i.ibb.co/nBc0m2x/Winterburg.png',
        'Field by the River': 'i.ibb.co/gZSnNpr/Field-by-the-river.png',
        'Legacy Town': 'i.ibb.co/ZM7GFMX/Legacy-Town.png'}


def generate(map):
    if map in MAPS.keys():
        return "[img width=250]" + MAPS[map] + "[/img]"
    else:
        return map
