from Exceptions import InvalidInputError

SMALL_SPACER = " " * 2
STAR_IMAGE_URL = "i.ibb.co/LDKmtQd/qAUpJsr.png"


def generate(star_of_the_week, congrats, star_img_url=STAR_IMAGE_URL):
    if star_of_the_week is None:
        raise InvalidInputError("Star of the week not present!")

    congrats_section = list()
    congrats_section.append(__create_star_of_the_week_line(star_of_the_week, star_img_url))

    if congrats:
        congrats_section.append("[b]CONGRATULATIONS TO:[/b]\n")

        for congrats_line in congrats:
            if congrats_line.names:
                congrats_section.append(__create_congrats_line(congrats_line))

    congrats_section.append("\n[hr][/hr]")
    return "".join(congrats_section)


def __create_star_of_the_week_line(star, star_img_url):
    return (
        "[hr][/hr]\n\n[center]"
        "[img]{star_img}[/img] "
        "[size=15px]"
        "Star of the week"
        "{spacer}[b]-[/b]{spacer}"
        "[b]{star_name}[/b]"
        "{spacer}[b]-[/b]{spacer}"
        "{star_kills} kills"
        "[/size]"
        "[img]{star_img}[/img]"
        "[/center]\n\n\n").format(
            spacer=SMALL_SPACER,
            star_img=star_img_url,
            star_name=star.name,
            star_kills=star.kills)


def __create_congrats_line(congrats):
    return (
        "([color=yellow][b]*[/b][/color]) {0} for making it to [i]{1}[/i]\n").format(
            __create_names_list(congrats.names), congrats.category)


def __create_names_list(names):
    names_list = list()
    names_list.append("{0}".format(__create_name(names[0])))
    for name in names[1:]:
        if name == names[-1]:
            names_list.append(__create_last_name(name))
        else:
            names_list.append(__create_middle_name(name))

    return "".join(names_list)


def __create_name(name):
    return "[b][i]{0}[/i][/b]".format(name)


def __create_middle_name(name):
    return ", {0}".format(__create_name(name))


def __create_last_name(name):
    return " and {0}".format(__create_name(name))
