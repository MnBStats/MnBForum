SPACER = " " * 25
SMALL_SPACER = " " * 2


def generate(header, spacer, small_spacer):
    return "[tr][td][b]PLAYER[/b]{spacer}[/td]{cells}[/tr]\n".format(
        spacer=spacer,
        cells=__generate_header_cells(header, small_spacer))


def __generate_header_cells(header, small_spacer):
    bb_cells = []
    for cell in header:
        bb_cells.append("[td]{spacer}[b]{cell}[/b]{spacer}[/td]".format(spacer=small_spacer, cell=cell))

    return "".join(bb_cells)
