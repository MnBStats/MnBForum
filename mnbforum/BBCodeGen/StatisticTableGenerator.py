from Exceptions import InvalidInputError


def generate(title, subtitle, content):
    if title is None or subtitle is None or content is None:
        raise InvalidInputError("StatisticTableGenerator: Invalid input!")

    return (
        "[table=noborder]\n"
        "{title_row}"
        "{row_with_line}"
        "[tr][td][spoiler]"
        "[center][table]\n"
        "{row_with_line}"
        "[tr][td]{content}[/td][/tr]\n"
        "{row_with_line}"
        "[/table][/center]"
        "[/spoiler][/td][/tr]\n"
        "[/table]").format(
            row_with_line="[tr][td][hr][/hr][/td][/tr]\n",
            title_row=__create_title_row(title, subtitle),
            content=content)


def __create_title_row(title, subtitle):
    return (
        "[tr][td]"
        "[center][size=21px]{title}[/size][/center]"
        "{subtitle_part}"
        "[/td][/tr]\n").format(title=title, subtitle_part=__create_subtitle_part(subtitle))


def __create_subtitle_part(subtitle):
    if subtitle:
        return "\n[center][size=11px][i]{0}[/i][/size][/center]".format(subtitle)

    return ""
