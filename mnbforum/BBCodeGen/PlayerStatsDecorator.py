from . import TableWithPlayersGenerator
from . import StatisticTableGenerator
import re


def generate_player_table(player_stats):
    stats_table =__generate_stats_table(player_stats)

    stats_table = __enumerate_stats(stats_table)
    stats_table = __insert_separator_lines(stats_table)

    return '[center]' + StatisticTableGenerator.generate(
        title="Player Stats",
        subtitle="",
        content=stats_table) + '[/center]'


def __generate_stats_table(player_stats):
    header = ["KILLS", "DEATHS", "TEAMKILLS"]
    first_cell_spacer = " " * 5

    if len(player_stats[0]) > 4:
        header.extend(["ASSISTS", "PLD"])

    return TableWithPlayersGenerator.generate(
        header=header,
        table=__center_small_fields(player_stats),
        spacer=first_cell_spacer,
        small_spacer="")


def __center_small_fields(player_stats):
    formatted_stats = []
    for entry in player_stats:
        centered_fields = ['[center]'+str(field)+'[/center]' for field in entry[1:]]
        entry = [entry[0]]
        entry.extend(centered_fields)
        formatted_stats.append(tuple(entry))

    return formatted_stats


def __enumerate_stats(stats_table):
    rows = re.findall('\[tr\](.*)\[/tr\]', stats_table)
    rows[0] = '[td][b]POS[/b]     [/td]' + rows[0]

    for pos, row in enumerate(rows[1:], 1):
        rows[pos] = '[td]'+str(pos)+'[/td]' + row

    return '[table=noborder]' + "".join(['[tr]'+row+'[/tr]\n' for row in rows]) + '[/table]'


def __insert_separator_lines(table):
    rows = re.findall('\[tr\].*\[/tr\]\n', table)
    separator_line = '[tr]' + '[td][hr][/hr][/td]' * len(re.findall('\[td\]', rows[0])) + '[/tr]\n'

    rows.insert(1, separator_line)
    rows.insert(12, separator_line)

    return '[table=noborder]' + "".join(rows) + '[/table]'

