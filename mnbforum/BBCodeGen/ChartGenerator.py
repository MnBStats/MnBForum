from pychart import *
import subprocess
import os
import re

theme.get_options()
theme.use_color = 1
theme.default_font_size = 14
theme.output_file = "chart.png"
theme.reinitialize()


class ChartGenerator:
    def __init__(self):
        pass

    @staticmethod
    def generate_kill_types_chart(stats, filename=""):
        kill_colours = {"swordone": color.red, "axeone": color.red2, "spear": color.darkorange,
                        "couchedlance": color.goldenrod, "horseimpact": color.yellow,
                        "bow": color.green, "crossbow": color.springgreen, "headshot": color.forestgreen, "javelin": color.blue,
                        "swordtwo": color.red3, "axetwo": color.red4, "blunt": color.mediumpurple, "morningstar": color.mediumorchid, "maul": color.purple, "axethrow": color.midnightblue, "punch": color.hotpink}

        kill_groups = {"a_onehanded": ("swordone", "axeone"),
                       "b_spear": ("spear", ),
                       "c_mounted": ("couchedlance", "horseimpact"),
                       "d_archer": ("bow", "crossbow"),
                       "e_headshot": ("headshot", ),
                       "f_throwing": ("javelin", "axethrow"),
                       "g_twohanded": ("swordtwo", "axetwo", "blunt", "morningstar", "maul"),
                       "h_unarmed": ("punch", )}

        chart_data = []
        for group in sorted(kill_groups):
            for kill in stats:
                if(kill[0] in kill_groups.get(group)):
                    chart_data.append(kill)

        chart_data = [(str(tup[0]) + " (" + str(tup[1]) + ")", tup[1]) for tup in chart_data]

        chart_area = area.T(size=(500, 500), bg_style=fill_style.Plain(bgcolor=color.T(r=228, g=217, b=197)), legend=None)
        plot = pie_plot.T(data=chart_data, arc_offsets=[5] * len(stats), fill_styles=[fill_style.Plain(bgcolor=kill_colours.get(tup[0].split(" ")[0])) for tup in chart_data], shadow=(2, -2, fill_style.gray50), label_offset=25)

        if filename != "":
            can = canvas.init(filename + ".png")

        chart_area.add_plot(plot)
        chart_area.draw()

        if filename != "":
            can.close()

        ChartGenerator.__make_transparent(filename)
        return ChartGenerator.__upload_image(filename)

    @staticmethod
    def __make_transparent(filename):
        if filename != "":
            os.system('convert ' + filename + '.png -transparent white ' + filename + '.png')
        else:
            os.system('convert chart.png -transparent white chart.png')


    @staticmethod
    def __upload_image(filename):
        if filename != "":
            img_data = subprocess.check_output("curl -si https://upload.gyazo.com/api/upload " \
                                 "-F 'access_token=f74512d9ab06064104d3583917810b2b57093ebb147776e21323cd315ed6a49c' "  \
                                 "-F 'imagedata=@/home/anja/MnBForum/mnbforum/" + filename + ".png'", shell=True)
        else:
            img_data = subprocess.check_output("curl -si https://upload.gyazo.com/api/upload " \
                                 "-F 'access_token=f74512d9ab06064104d3583917810b2b57093ebb147776e21323cd315ed6a49c' "  \
                                 "-F 'imagedata=@/home/anja/MnBForum/mnbforum/chart.png'", shell=True)

        return re.search('"url":"(.*)"}', img_data.decode('utf8')).group(1)

