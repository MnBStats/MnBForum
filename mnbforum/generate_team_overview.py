from __init__ import TOURNAMENT_NAME
from PROVIDERS.TournamentProvider import TournamentProvider
from PROVIDERS.TeamProvider import TeamProvider
from PROVIDERS.TeamStatsProvider import TeamStatsProvider
from BBCodeGen import MiniMapGenerator
from BBCodeGen import FactionThumbnailGenerator
from BBCodeGen.ChartGenerator import ChartGenerator


class TeamStatsGenerator():
    def __init__(self):
        self.tournament_provider = TournamentProvider()
        self.team_provider = TeamProvider()

        self.tournament = self.tournament_provider.provide_tournament(TOURNAMENT_NAME)
        self.teams = self.team_provider.provide_tournament_teams(self.tournament.tournament_id)

    def generate_header(self):
        header = ("[tr]"
                  "[td][b]TEAM[/b][/td][td]{spacer}[/td]"
                  "[td][b]TOTAL KILLS[/b][/td][td]{spacer}[/td]"
                  "[td][b]TEAM K/D[/b][/td][td]{spacer}[/td]"
                  "[td][b]STRONGEST MAP[/b][/td][td]{spacer}[/td]"
                  "[td][b]KILL TYPE DISTRIBUTION[/b][/td][td]{spacer}[/td]"
                  "[td][b]STRONGEST FACTION[/b][/td][td]{spacer}[/td]"
                  "[td][b]PLD[/b][/td][td]{spacer}[/td]"
                  "[/tr]"
                  "[tr]{line_row}[/tr]").format(
                      spacer=" " * 5,
                      line_row="[td][hr][/td][td]     [/td]" * 7)

        return header

    def add_kd_colour(self, kd):
        if kd < 1:
            return "[glow=red,2,300]" + str(kd) + "[/glow]"

        elif kd == 1:
            return "[glow=yellow,2,300]" + str(kd) + "[/glow]"

        else:
            return "[glow=green,2,300]" + str(kd) + "[/glow]"

    def generate_stats_for_team(self, team):
        stats_provider = TeamStatsProvider(team)

        name = team.name
        total_kills = stats_provider.provide_total_kills()
        kd = stats_provider.provide_kd()
        number_of_matches = stats_provider.provide_number_of_matches()

        strongest_map = stats_provider.provide_strongest_map()
        strongest_faction = stats_provider.provide_strongest_faction()

        team_stats = ("[glow=grey,2,300]{name}[/glow]".format(name=name),
                      "[center]{kills}[/center]".format(kills=str(total_kills)),
                      "[center]{kd}[/center]".format(kd=self.add_kd_colour(kd)),
                      "{map}".format(map=MiniMapGenerator.generate(strongest_map)),
                      "[center][img width=250]{chart}[/img][center]".format(chart="<chart.png>"),
                      "[center]{faction}[/center]".format(faction=FactionThumbnailGenerator.generate(strongest_faction)),
                      "[center]{pld}[/center]".format(pld=str(number_of_matches)))

        # ChartGenerator.generate_kill_types_chart(stats_provider.provide_kill_types(), str(team.tag))

        return team_stats


if __name__ == '__main__':
    generator = TeamStatsGenerator()

    print("[hr]\n[table]")
    print(generator.generate_header())

    for team in generator.teams:
        print("[tr]")

        team_stats = generator.generate_stats_for_team(team)
        for stat in team_stats:
            print ("[td]{stat}[/td][td]{spacer}[/td]".format(
                spacer=" " * 5,
                stat=stat))

        print("[/tr]")

    print("[/table]\n[hr]")
