TOURNAMENT_NAME = "ECS4"
WEEK_NUMBER = "1"

DIV_A = {'team1': 'La 7e Compagnie',
         'team2': 'Backline Tactics',
         'team3': 'Belediye Sovalyeleri',
         'team4': 'Boynuzlu Kurtlar Partisi',
         'team5': 'Commando OIDA',
         'team6': 'Dorstons'}
DIV_B = {'team7': 'Druberfahrn',
         'team8': 'Dzikie Weze',
         'team9': 'Galya',
         'team10': 'Godsent',
         'team11': 'Highest Virtue',
         'team12': 'Kebab Tactics',
         'team13': 'Milites Fortunae',
         'team14': 'Schwertbruderorden'}
DIV_C = {'team15': 'Snus Gang',
         'team16': 'The Real GIGASTACK',
         'team17': 'Tilted Stabbers',
         'team18': 'Time Travelers',
         'team19': 'Warriors and Champions'}
