from PROVIDERS.FinalsStatsProvider import FinalsStatsProvider
from BBCodeGen import TableWithPlayersGenerator
from BBCodeGen import StatisticTableGenerator
from BBCodeGen.ChartGenerator import ChartGenerator


class FinalsMatchAnalyser():
    def __init__(self, match_id):
        self.match_id = match_id
        self.stats_provider = FinalsStatsProvider(match_id)

    def generate_spawn_header(self, attacking_team, defending_team, score, spawn_number):
        header = ("[size=14pt]SPAWN {spawn_no} [/size]\n"
                  "{line}\n"
                  "[center][size=14pt]{team_a}{big_spacer}{team_a_score}{small_spacer}-{small_spacer}{team_b_score}{big_spacer}{team_b}[/size][/center]"
                  "{line}").format(
                      line="[hr]",
                      big_spacer=" " * 45,
                      small_spacer=" " * 5,
                      spawn_no=str(spawn_number),
                      team_a=attacking_team,
                      team_a_score=score.home,
                      team_b_score=score.away,
                      team_b=defending_team)

        return header

    def add_kd_colour(self, kd):
        if kd < 1:
            return "[glow=red,2,300]" + str(kd) + "[/glow]"

        elif kd == 1:
            return "[glow=yellow,2,300]" + str(kd) + "[/glow]"

        else:
            return "[glow=green,2,300]" + str(kd) + "[/glow]"

    def format_kill_list(self, kill_list):
        formatted_kill_list = []

        for kill in kill_list:
            formatted_kill_list.append("[i]"+kill[0]+"[/i]" + " " + "[glow=grey,2,300]"+str(kill[1])+"[/glow]")

        return ", ".join(formatted_kill_list)

    def format_player_stats(self, player_stats):
        header = ["K", "D", "TK"]

        if len(player_stats[0]) > 4:
            header.extend(["A", "Classes"])

        first_cell_spacer = " " * 5
        stats_table = TableWithPlayersGenerator.generate(header=header, table=player_stats, spacer=first_cell_spacer)

        return StatisticTableGenerator.generate("Player Stats", "Complete player stats for the current spawn.", stats_table)

    def generate_team_stats_for_spawn(self, team, spawn_number):
        player_stats = self.stats_provider.provide_team_stats_for_spawn(team, spawn_number)
        team_kd = self.stats_provider.provide_team_kd_for_spawn(team, spawn_number)
        elite_stats = self.stats_provider.provide_team_elite_stats_for_spawn(team, spawn_number)
        chart_data, class_percentages = self.stats_provider.provide_team_kill_distribution_section_for_spawn(team, spawn_number)
        tk_rounds, adv_rounds, disadv_rounds, comeback_rounds = self.stats_provider.provide_spawn_analysis_for_team(team, spawn_number)

        team_stats = ("[center]{player_stats}[/center]\n\n"
                      "[center][size=12pt]TEAM K/D = {kd}[/size][/center]\n\n"
                      "[b]MELEE KILLS:[/b] {melee_kills}\n"
                      "[b]COUCHED LANCES:[/b] {couched_lances}\n"
                      "[b]HEADSHOTS:[/b] {headshots}\n"
                      "[b]KILL STREAKS:[/b] {kill_streaks}\n\n\n\n"
                      "[center][img witdh=250]{chart}[/img][/center]\n\n"
                      "[center][color=red][i][b]Infantry Kills:[/b][/i][/color] [i]{team_melee_kills}[/i]%[/center]\n"
                      "[center][color=green][i][b]Archer Kills:[/b][/i][/color] [i]{team_ranged_kills}[/i]%[/center]\n"
                      "[center][glow=yellow,2,300][i][b]Cavalry Kills:[/b][/i][/glow] [i]{team_cav_kills}[/i]%[/center]\n\n\n"
                      "[b]ROUNDS WITH TK:[/b]  {tk_rounds}  [b]LOST:[/b]  <#> [i](<#>%)[/i]\n"
                      "[b]ROUNDS WITH EARLY ADVANTAGE:[/b]  {adv_rounds}  [b]WON:[/b]  <#> [i](<#>%)[/i]\n"
                      "[b]ROUNDS WITH EARLY DISADVANTAGE:[/b]  {disadv_rounds}  [b]LOST:[/b]  <#> [i](<#>%)[/i]\n"
                      "[b]BIGGEST DISADVANTAGE TEAM MADE A COMEBACK FROM:[/b]  {comeback_rounds}").format(
                          player_stats=self.format_player_stats(player_stats),
                          kd=self.add_kd_colour(team_kd),
                          melee_kills=self.format_kill_list(elite_stats[0]),
                          couched_lances=self.format_kill_list(elite_stats[1]),
                          headshots=self.format_kill_list(elite_stats[2]),
                          kill_streaks=self.format_kill_list(elite_stats[3]),
                          chart="<chart.png>",
                          team_melee_kills=class_percentages[0],
                          team_ranged_kills=class_percentages[1],
                          team_cav_kills=class_percentages[2],
                          tk_rounds=tk_rounds,
                          adv_rounds=adv_rounds,
                          disadv_rounds=disadv_rounds,
                          comeback_rounds=comeback_rounds)

        ChartGenerator.generate_kill_types_chart(chart_data)

        return team_stats

if __name__ == "__main__":
    analyser = FinalsMatchAnalyser(159) # manual entry temporarily; should fix to automatically recognise the finals
    spacer = " " * 50

    for spawn in range(1, analyser.stats_provider.provide_number_of_spawns()+1):
        attacking_team, defending_team, score = analyser.stats_provider.provide_spawn_info(spawn)
        print(analyser.generate_spawn_header(attacking_team.name, defending_team.name, score, spawn))

        print ("[center][table]" +
                   "[tr]" +
                       "[td]" + analyser.generate_team_stats_for_spawn(attacking_team, spawn) + "[/td]" +
                       "[td]" + spacer + "[/td]"
                       "[td]" + analyser.generate_team_stats_for_spawn(defending_team, spawn) + "[/td]" +
                   "[/tr]" +
               "[/table][/center]\n\n\n")
