class MatchStats():
    def __init__(self, match, team_a_kills, team_b_kills):
        self.match = match
        self.team_a_kills = team_a_kills
        self.team_b_kills = team_b_kills

    def title(self):
        return self.match.home_team.name + " vs " + self.match.away_team.name

