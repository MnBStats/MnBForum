class Match():
    def __init__(self, match_id, tournament_id, team_a, team_b, home_score, away_score):
        self.match_id = match_id
        self.tournament_id = tournament_id
        self.home_team = team_a
        self.away_team = team_b
        self.home_team_score = home_score
        self.away_team_score = away_score
