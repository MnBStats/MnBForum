class Team():
    def __init__(self, name, tag, team_id, tournament_id):
        self.name = name
        self.tag = tag
        self.team_id = team_id
        self.tournament_id = tournament_id

    def __cmp__(self, other):
        return int(self.team_id) - int(other.team_id)

    def __hash__(self):
        return hash((self.name, self.team_id))

    def __eq__(self, other):
        return (self.name == other.name
           and self.tag == other.tag
           and self.team_id == other.team_id
           and self.tournament_id == other.tournament_id)

    def __str__(self):
        return "Name: {0} | Tag: {1} | Id: {2} | TournamentId: {3}\n".format(
            self.name,
            self.tag,
            self.team_id,
            self.tournament_id)

    def __repr__(self):
        return self.__str__()
