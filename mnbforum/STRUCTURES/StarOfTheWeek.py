class StarOfTheWeek():
    def __init__(self, name, kills):
        self.name = name
        self.kills = kills

    def __str__(self):
        return "{} {}".format(
            self.name,
            self.kills)

    def __repr__(self):
        return self.__str__()
