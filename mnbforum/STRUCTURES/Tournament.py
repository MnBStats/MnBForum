class Tournament():
    def __init__(self, name, tournament_id):
        self.name = name
        self.tournament_id = tournament_id

    def __eq__(self, other):
        if not isinstance(other, Tournament):
            raise TypeError(other)

        return self.name == other.name and self.tournament_id == other.tournament_id
