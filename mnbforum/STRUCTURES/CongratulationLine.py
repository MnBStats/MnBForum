class CongratulationLine():
    def __init__(self, names, category):
        self.names = names
        self.category = category

    def __str__(self):
        return "{} {}".format(
            self.category,
            self.names)
