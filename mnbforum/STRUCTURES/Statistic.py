class Statistic():
    def __init__(self, title, subtitle, statistic_type, stats):
        self.title = title
        self.subtitle = subtitle
        self.statistic_type = statistic_type
        self.stats = stats

    def __str__(self):
        return "{} {} {} {}".format(
            self.title,
            self.subtitle,
            self.statistic_type,
            self.stats)

    def __repr__(self):
        return self.__str__()
